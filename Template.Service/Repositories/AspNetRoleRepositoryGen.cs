using IPM.Service.Infrastructure;
using IPM.Service.Entities;

namespace IPM.Service.Repositories
{
    public partial class AspNetRoleRepository : BaseRepository<AspNetRole>, IAspNetRoleRepository
    {
        public AspNetRoleRepository(System.Data.Entity.DbContext dbContext) : base(dbContext)
        {
        }
    }

    public partial interface IAspNetRoleRepository : IBaseRepository<AspNetRole>
    {
    }
}
