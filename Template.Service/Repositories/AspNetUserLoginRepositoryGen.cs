using IPM.Service.Infrastructure;
using IPM.Service.Entities;

namespace IPM.Service.Repositories
{
    public partial class AspNetUserLoginRepository : BaseRepository<AspNetUserLogin>, IAspNetUserLoginRepository
    {
        public AspNetUserLoginRepository(System.Data.Entity.DbContext dbContext) : base(dbContext)
        {
        }
    }

    public partial interface IAspNetUserLoginRepository : IBaseRepository<AspNetUserLogin>
    {
    }
}
