using IPM.Service.Infrastructure;
using IPM.Service.Entities;

namespace IPM.Service.Repositories
{
    public partial class AspNetUserRepository : BaseRepository<AspNetUser>, IAspNetUserRepository
    {
        public AspNetUserRepository(System.Data.Entity.DbContext dbContext) : base(dbContext)
        {
        }
    }

    public partial interface IAspNetUserRepository : IBaseRepository<AspNetUser>
    {
    }
}
