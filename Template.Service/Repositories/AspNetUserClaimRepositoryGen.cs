using IPM.Service.Infrastructure;
using IPM.Service.Entities;

namespace IPM.Service.Repositories
{
    public partial class AspNetUserClaimRepository : BaseRepository<AspNetUserClaim>, IAspNetUserClaimRepository
    {
        public AspNetUserClaimRepository(System.Data.Entity.DbContext dbContext) : base(dbContext)
        {
        }
    }

    public partial interface IAspNetUserClaimRepository : IBaseRepository<AspNetUserClaim>
    {
    }
}
