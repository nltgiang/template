﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service
{
    public enum ProcessStatusEnum
    {
        Open = 1,
        InProcess = 2,
        Pending = 3,
        Cancelled = 4,
        Close = 5,
    }
    public enum HistoryOperateTypeEnum
    {
        Create = 1,
        Update = 2,
        Delete = 3,
    }
}
