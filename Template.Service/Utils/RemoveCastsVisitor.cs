﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Utils
{
   public class RemoveCastsVisitor : ExpressionVisitor
    {
        public static RemoveCastsVisitor Default = new RemoveCastsVisitor();

        private RemoveCastsVisitor()
        {

        }
        protected override Expression VisitUnary(UnaryExpression node)
        {
            if (node.NodeType == ExpressionType.Convert && node.Type.IsAssignableFrom(node.Operand.Type))
            {
                return base.Visit(node.Operand);
            }
            return base.VisitUnary(node);
        }
    }
}
