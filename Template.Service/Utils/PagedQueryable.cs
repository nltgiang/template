﻿using System.Linq;

namespace IPM.Service.Utils
{
    public class PagedQueryable<T>
    {
        private IQueryable<T> item;

        public PagedQueryable(int page, int pageLength)
        {
            this.Page = page;
            this.PageSize = pageLength;
        }

        public IQueryable<T> Items
        {
            get { return this.item; }
            set
            {
                this.item = value.Skip(this.Start).Take(this.PageSize);
            }
        }

        public int PageSize { get; set; }

        public int Page { get; set; }

        public int TotalQueriedItems { get; set; }

        public int TotalItems { get; set; }

        public int Start
        {
            get
            {
                return (this.Page - 1) * this.PageSize;
            }
        }

        public int End
        {
            get
            {
                return this.Page * this.PageSize;
            }
        }

        public int TotalPage
        {
            get
            {
                return (this.TotalQueriedItems + this.PageSize - 1) / this.PageSize;
            }
        }
    }
}