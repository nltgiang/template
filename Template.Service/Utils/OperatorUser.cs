﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Utils
{
    public class OperatorUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public DateTime OperateTime { get; set; }
    }
}
