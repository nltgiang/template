﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Utils
{
    public static class ExtentionUtils
    {
        /// <summary>
        /// Sort squence with boolean direction
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <param name="isAssending"></param>
        /// <returns></returns>
        public static IQueryable<TSource> Sort<TSource, TKey>(
            this IQueryable<TSource> source, 
            Expression<Func<TSource, TKey>> keySelector, 
            bool isAssending)
        {
            if (isAssending)
            {
                return source.OrderBy(keySelector);
            }
            else
            {
                return source.OrderByDescending(keySelector);
            }
        }

    }
}
