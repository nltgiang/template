﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Service.Utils
{
    public static class DependencyUtils
    {
        /// <summary>
        /// Get instance of class from factory
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>Instance</returns>
        public static T Resolve<T>() where T : class
        {
            return DependencyResolverExtensions.GetService<T>(DependencyResolver.Current);
        }
    }
}