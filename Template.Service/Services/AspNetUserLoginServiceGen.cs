using IPM.Service.Utils;
using IPM.Service.Entities;
using IPM.Service.Infrastructure;

namespace IPM.Service.Services
{
    public partial class AspNetUserLoginService : BaseService<AspNetUserLogin>, IAspNetUserLoginService
    {
        public AspNetUserLoginService(IUnitOfWork unitOfWork, IBaseRepository<AspNetUserLogin> repository, OperatorUser user) 
				: base(unitOfWork, repository, user)
        {
        }
    }

    public partial interface IAspNetUserLoginService : IBaseService<AspNetUserLogin>
    {
    }
}
