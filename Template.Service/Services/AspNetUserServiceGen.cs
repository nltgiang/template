using IPM.Service.Utils;
using IPM.Service.Entities;
using IPM.Service.Infrastructure;

namespace IPM.Service.Services
{
    public partial class AspNetUserService : BaseService<AspNetUser>, IAspNetUserService
    {
        public AspNetUserService(IUnitOfWork unitOfWork, IBaseRepository<AspNetUser> repository, OperatorUser user) 
				: base(unitOfWork, repository, user)
        {
        }
    }

    public partial interface IAspNetUserService : IBaseService<AspNetUser>
    {
    }
}
