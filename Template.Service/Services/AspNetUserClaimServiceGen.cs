using IPM.Service.Utils;
using IPM.Service.Entities;
using IPM.Service.Infrastructure;

namespace IPM.Service.Services
{
    public partial class AspNetUserClaimService : BaseService<AspNetUserClaim>, IAspNetUserClaimService
    {
        public AspNetUserClaimService(IUnitOfWork unitOfWork, IBaseRepository<AspNetUserClaim> repository, OperatorUser user) 
				: base(unitOfWork, repository, user)
        {
        }
    }

    public partial interface IAspNetUserClaimService : IBaseService<AspNetUserClaim>
    {
    }
}
