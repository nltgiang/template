using IPM.Service.Utils;
using IPM.Service.Entities;
using IPM.Service.Infrastructure;

namespace IPM.Service.Services
{
    public partial class AspNetRoleService : BaseService<AspNetRole>, IAspNetRoleService
    {
        public AspNetRoleService(IUnitOfWork unitOfWork, IBaseRepository<AspNetRole> repository, OperatorUser user) 
				: base(unitOfWork, repository, user)
        {
        }
    }

    public partial interface IAspNetRoleService : IBaseService<AspNetRole>
    {
    }
}
