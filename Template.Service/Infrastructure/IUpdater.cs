﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public interface IUpdater
    {
        string Updater { get; set; }
    }
}
