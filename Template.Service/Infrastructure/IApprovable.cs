﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public interface IApprovable
    {
        bool Approved { get; set; }
    }
}
