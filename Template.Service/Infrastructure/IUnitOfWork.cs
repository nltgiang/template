﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        int Commit();
        void BeginTransaction();
        int CommitTransaction();
        Task<int> CommitAsync();
        Task<int> CommitTransactionAsync();
    }
}
