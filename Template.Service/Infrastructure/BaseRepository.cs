﻿using IPM.Service.Utils;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected readonly DbContext dbContext;

        protected readonly DbSet<TEntity> dbSet;

        public BaseRepository(DbContext context)
        {
            //initial db context and db set
            this.dbContext = context;
            this.dbSet = this.dbContext.Set<TEntity>();
        }

        public void Approve(TEntity entity)
        {
            //check existing of Approved property
            if (entity is IApprovable)
            {
                //update value and save to db
                (entity as IApprovable).Approved = true;
                this.Edit(entity);
                return;
            }

            throw new NotSupportedException("TEntity must containt Approved property to use this method. TEntity: "
                + typeof(TEntity).FullName);
        }

        public void Activate(TEntity entity)
        {
            //check existing of Active property
            if (entity is IActivable)
            {
                //update value and save to db
                (entity as IActivable).Active = true;
                this.Edit(entity);
                return;
            }

            throw new NotSupportedException("TEntity must containt Active property to use this method. TEntity: "
                + typeof(TEntity).FullName);
        }

        public void Deactivate(TEntity entity)
        {
            //check existing of Active property
            if (entity is IActivable)
            {
                //update value and save to db
                (entity as IActivable).Active = false;
                this.Edit(entity);
                return;
            }
            throw new NotSupportedException("TEntity must containt Active property to use this method. TEntity: "
                + typeof(TEntity).FullName);
        }

        public void Add(TEntity entity)
        {
            //save entity to db
            this.dbSet.Add(entity);
        }

        public void Edit(TEntity entity)
        {
            //change entity state to force update this entity
            this.dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(TEntity entity)
        {
            //check existing of Deleted property
            if (entity is IDeletable)
            {
                //update delete flag, not completely remove from db
                (entity as IDeletable).Deleted = true;
                return;
            }
            //remove from db
            this.dbSet.Remove(entity);
        }

        public TEntity FirstOrDefault()
        {
            //get first entity
            var result = this.GetAll().FirstOrDefault();
            return CheckLogicalExisting(result);
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            //get first entity with linq query
            var result = this.GetAll().FirstOrDefault(predicate);
            return CheckLogicalExisting(result);
        }

        public async Task<TEntity> FirstOrDefaultAsync()
        {
            //get first entity
            var result = await this.GetAll().FirstOrDefaultAsync();
            return CheckLogicalExisting(result);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            //get first entity with linq query
            var result = await this.GetAll().FirstOrDefaultAsync(predicate);
            return CheckLogicalExisting(result);
        }

        public TEntity FirstOrDefaultActive()
        {
            //get first entity including logical existing check
            var result = FirstOrDefault();
            //re-check existing, if exist, find active flag
            if (result != null && result is IActivable)
            {
                //check active flag value
                if ((result as IActivable).Active == false)
                {
                    //remove inactive result
                    result = null;
                }
            }
            return result;
        }

        public TEntity FirstOrDefaultActive(Expression<Func<TEntity, bool>> predicate)
        {
            //get first entity with query including logical existing check
            var result = FirstOrDefault(predicate);
            //re-check existing, if exist, find active flag
            if (result != null && result is IActivable)
            {
                //check active flag value
                if (!(result as IActivable).Active)
                {
                    //remove inactive result
                    result = null;
                }
            }
            return result;
        }

        public async Task<TEntity> FirstOrDefaultActiveAsync()
        {
            //get first entity including logical existing check
            var result = await FirstOrDefaultAsync();
            //re-check existing, if exist, find active flag
            if (result != null && result is IActivable)
            {
                //check active flag value
                if (!(result as IActivable).Active)
                {
                    //remove inactive result
                    result = null;
                }
            }
            return result;
        }

        public async Task<TEntity> FirstOrDefaultActiveAsync(Expression<Func<TEntity, bool>> predicate)
        {
            //get first entity with query including logical existing check
            var result = await FirstOrDefaultAsync(predicate);
            //re-check existing, if exist, find active flag
            if (result != null && result is IActivable)
            {
                //check active flag value
                if (!(result as IActivable).Active)
                {
                    //remove inactive result
                    result = null;
                }
            }
            return result;
        }

        public IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> result;
            //check the containing of deleted flag in this entity type
            if (typeof(IDeletable).IsAssignableFrom(typeof(TEntity)))
            {
                //set default query is get logical existed data
                Expression<Func<TEntity, bool>> query = e => !((IDeletable)e).Deleted;
                //remove casting for linq query
                query = RemoveCastsVisitor.Default.Visit(query) as Expression<Func<TEntity, bool>>;
                //execute query
                result = this.dbSet.Where(query);
            }
            else
            {
                //get all data from db
                result = this.dbSet.AsQueryable();
            }
            return result;
        }

        public IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> userQuery)
        {
            IQueryable<TEntity> result;
            result = this.GetAll() //get all data including logical existing check first
                .Where(userQuery); //get data by user query
            return result;
        }

        public TEntity GetById<TKey>(TKey id)
        {
            //find entity with inputted id
            var entity = this.dbSet.Find(new object[] { id });
            return CheckLogicalExisting(entity);
        }

        public async Task<TEntity> GetByIdAsync<TKey>(TKey id)
        {
            //find entity with inputted id
            var entity = await this.dbSet.FindAsync(new object[] { id });
            return CheckLogicalExisting(entity);
        }

        public IQueryable<TEntity> GetAllActive()
        {
            if (typeof(IActivable).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> expression;
                if (typeof(IDeletable).IsAssignableFrom(typeof(TEntity)))
                {
                    expression = (TEntity q) => ((IActivable)q).Active && !((IDeletable)q).Deleted;
                }
                else
                {
                    expression = (TEntity q) => ((IActivable)q).Active;
                }
                expression = RemoveCastsVisitor.Default.Visit(expression) as Expression<Func<TEntity, bool>>;
                return this.dbSet.Where(expression);
            }
            return this.GetAll();
        }

        public IQueryable<TEntity> GetManyActive(Expression<Func<TEntity, bool>> userQuery)
        {
            IQueryable<TEntity> result;
            result = this.GetAllActive() //get all data including logical existing check first
                .Where(userQuery); //get data by user query
            return result;
        }

        public void Refresh(TEntity entity)
        {
            this.dbContext.Entry(entity).Reload();
        }

        public IQueryable<TEntity> GetAllApproved()
        {
            if (typeof(IApprovable).IsAssignableFrom(typeof(TEntity)))
            {
                Expression<Func<TEntity, bool>> expression;
                if (typeof(IDeletable).IsAssignableFrom(typeof(TEntity)))
                {
                    expression = (TEntity q) => ((IApprovable)q).Approved && !((IDeletable)q).Deleted;
                }
                else
                {
                    expression = (TEntity q) => ((IApprovable)q).Approved;
                }
                expression = RemoveCastsVisitor.Default.Visit(expression) as Expression<Func<TEntity, bool>>;
                return this.dbSet.Where(expression);
            }
            return this.GetAll();
        }


        public IQueryable<TEntity> GetManyApproved(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> result;
            result = this.GetAllApproved() //get all data including logical existing check first
                .Where(predicate); //get data by user query
            return result;
        }
        private TEntity CheckLogicalExisting(TEntity entity)
        {
            if (entity != null && entity is IDeletable)
            {
                //check logical existing
                if ((entity as IDeletable).Deleted)
                {
                    //remove result
                    entity = null;
                }
            }
            return entity;
        }
    }
}
