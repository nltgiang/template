﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public interface IBaseService<TEntity> where TEntity : class, IEntity
    {
        TEntity FindById<TKey>(TKey id);

        IQueryable<TEntity> Get();

        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> GetActive();

        IQueryable<TEntity> GetActive(Expression<Func<TEntity, bool>> predicate);

        IQueryable<TEntity> GetApproved();

        IQueryable<TEntity> GetApproved(Expression<Func<TEntity, bool>> predicate);

        TEntity FirstOrDefault();

        TEntity FirstOrDefaultActive();

        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);

        TEntity FirstOrDefaultActive(Expression<Func<TEntity, bool>> predicate);

        bool Create(TEntity entity);

        bool Activate(TEntity entity);

        bool Deactivate(TEntity entity);

        bool Update(TEntity entity);

        bool Delete(TEntity entity);

        bool Save();

        void Refresh(TEntity entity);

        Task<TEntity> FindByIdAsync<TKey>(TKey id);

        Task<TEntity> FirstOrDefaultAsync();

        Task<TEntity> FirstOrDefaultActiveAsync();

        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> FirstOrDefaultActiveAsync(Expression<Func<TEntity, bool>> predicate);

        Task<bool> CreateAsync(TEntity entity);

        Task<bool> ActivateAsync(TEntity entity);

        Task<bool> DeactivateAsync(TEntity entity);

        Task<bool> UpdateAsync(TEntity entity);

        Task<bool> DeleteAsync(TEntity entity);

        Task<bool> DeleteAsync(int id);

        Task<bool> SaveAsync();
    }
}
