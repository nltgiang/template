﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private DbContext dbContext;
        private DbContextTransaction transaction;

        public UnitOfWork(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void BeginTransaction()
        {
            this.transaction = this.dbContext
                .Database
                .BeginTransaction();
        }

        public int Commit()
        {
            return this.dbContext.SaveChanges();
        }

        public Task<int> CommitAsync()
        {
            return this.dbContext.SaveChangesAsync();
        }

        public int CommitTransaction()
        {
            if (this.transaction == null)
            {
                return this.Commit();
            }
            try
            {
                var result = this.dbContext.SaveChanges();
                this.transaction.Commit();
                return result;
            }
            catch (Exception)
            {
                this.transaction.Rollback();
                throw;
            }
        }

        public async Task<int> CommitTransactionAsync()
        {
            if (this.transaction == null)
            {
                return this.Commit();
            }
            try
            {
                var result = await this.dbContext.SaveChangesAsync();
                this.transaction.Commit();
                this.transaction = null;
                return result;
            }
            catch (Exception)
            {
                this.transaction.Rollback();
                throw;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing && this.dbContext != null)
            {
                this.dbContext.Dispose();
                this.dbContext = null;
            }
        }
    }
}
