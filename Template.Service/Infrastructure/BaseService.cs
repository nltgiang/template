﻿using IPM.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public abstract class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class, IEntity
    {
        protected IUnitOfWork unitOfWork;

        protected OperatorUser operatorUser;

        protected IBaseRepository<TEntity> repository;

        public BaseService(IUnitOfWork unitOfWork, IBaseRepository<TEntity> repository, OperatorUser user)
        {
            this.unitOfWork = unitOfWork;
            this.repository = repository;
            this.operatorUser = user;
        }

        public virtual TEntity FindById<TKey>(TKey id)
        {
            return this.repository.GetById(id);
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.GetMany(predicate);
        }

        public virtual IQueryable<TEntity> GetActive()
        {
            return this.repository.GetAllActive();
        }

        public IQueryable<TEntity> GetActive(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.GetManyActive(predicate);
        }

        public IQueryable<TEntity> GetApproved()
        {
            return this.repository.GetAllApproved();
        }

        public IQueryable<TEntity> GetApproved(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.GetManyApproved(predicate);
        }

        public TEntity FirstOrDefault()
        {
            return this.repository.FirstOrDefault();
        }

        public TEntity FirstOrDefaultActive()
        {
            return this.repository.FirstOrDefaultActive();
        }

        public TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.FirstOrDefault(predicate);
        }

        public TEntity FirstOrDefaultActive(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.FirstOrDefaultActive(predicate);
        }

        public virtual bool Create(TEntity entity)
        {
            this.OnCreate(entity);
            return this.Save();
        }

        public virtual bool Update(TEntity entity)
        {
            this.OnUpdate(entity);
            return this.Save();
        }

        public virtual bool Delete(TEntity entity)
        {
            this.OnDelete(entity);
            return this.Save();
        }

        public virtual IQueryable<TEntity> Get()
        {
            return this.repository.GetAll();
        }

        public virtual bool Activate(TEntity entity)
        {
            this.OnActivate(entity);
            return this.Save();
        }

        public virtual bool Deactivate(TEntity entity)
        {
            this.OnDeactivate(entity);
            return this.Save();
        }

        public virtual bool Save()
        {
            return this.unitOfWork.CommitTransaction() > 0;
        }

        public virtual void Refresh(TEntity entity)
        {
            this.repository.Refresh(entity);
        }

        public Task<TEntity> FindByIdAsync<TKey>(TKey id)
        {
            return this.repository.GetByIdAsync<TKey>(id);
        }

        public Task<TEntity> FirstOrDefaultAsync()
        {
            return this.repository.FirstOrDefaultAsync();
        }

        public Task<TEntity> FirstOrDefaultActiveAsync()
        {
            return this.repository.FirstOrDefaultActiveAsync();
        }

        public Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.FirstOrDefaultAsync(predicate);
        }

        public Task<TEntity> FirstOrDefaultActiveAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return this.repository.FirstOrDefaultActiveAsync(predicate);
        }

        public async Task<bool> SaveAsync()
        {
            return await this.unitOfWork.CommitTransactionAsync() > 0;
        }

        public Task<bool> CreateAsync(TEntity entity)
        {
            this.OnCreate(entity);
            return this.SaveAsync();
        }

        public Task<bool> ActivateAsync(TEntity entity)
        {
            this.OnActivate(entity);
            return this.SaveAsync();
        }

        public Task<bool> DeactivateAsync(TEntity entity)
        {
            this.OnDeactivate(entity);
            return this.SaveAsync();
        }

        public Task<bool> UpdateAsync(TEntity entity)
        {
            this.OnUpdate(entity);
            return this.SaveAsync();
        }

        public Task<bool> DeleteAsync(TEntity entity)
        {
            this.OnDelete(entity);
            return this.SaveAsync();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await this.repository.GetByIdAsync(id);
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }
            this.OnDelete(entity);
            return await this.SaveAsync();
        }

        protected virtual void OnCreate(TEntity entity)
        {
            if (entity is IActivable)
            {
                (entity as IActivable).Active = true;
            }
            if (entity is IDeletable)
            {
                (entity as IDeletable).Deleted = false;
            }
            if (entity is IApprovable)
            {
                (entity as IApprovable).Approved = false;
            }
            if (entity is ICreateDate)
            {
                (entity as ICreateDate).CreateDate = DateTime.Now;
            }
            if (entity is ICreateTime)
            {
                (entity as ICreateTime).CreateTime = DateTime.Now;
            }
            if (entity is ILastChange)
            {
                (entity as ILastChange).LastChange = DateTime.Now;
            }
            if (entity is ICreater)
            {
                if (operatorUser != null)
                {
                    (entity as ICreater).Creater = operatorUser.Id;
                }
            }
            this.repository.Add(entity);
        }

        protected virtual void OnUpdate(TEntity entity)
        {
            if (entity is IUpdateDate)
            {
                (entity as IUpdateDate).UpdateDate = DateTime.Now;
            }
            if (entity is ILastChange)
            {
                (entity as ILastChange).LastChange = DateTime.Now;
            }
            if (entity is IUpdater)
            {
                if (operatorUser != null)
                {
                    (entity as IUpdater).Updater = operatorUser.Id;
                }
            }
            this.repository.Edit(entity);
        }

        protected virtual void OnActivate(TEntity entity)
        {
            this.repository.Activate(entity);
        }

        protected virtual void OnDeactivate(TEntity entity)
        {
            this.repository.Deactivate(entity);
        }

        protected virtual void OnDelete(TEntity entity)
        {
            this.repository.Delete(entity);
        }
    }
}
