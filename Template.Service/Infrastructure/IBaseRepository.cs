﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace IPM.Service.Infrastructure
{
    public interface IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        /// <summary>
        /// Approve an entity
        /// </summary>
        /// <param name="entity">entity has Approved property</param>
        void Approve(TEntity entity);
        /// <summary>
        /// Activate an entity
        /// </summary>
        /// <param name="entity">entity has Active property</param>
        void Activate(TEntity entity);
        /// <summary>
        /// Deactivate an entity
        /// </summary>
        /// <param name="entity">entity has Active property</param>
        void Deactivate(TEntity entity);
        /// <summary>
        /// Insert entity to db
        /// </summary>
        /// <param name="entity">entity</param>
        void Add(TEntity entity);
        /// <summary>
        /// Update entity change to db
        /// </summary>
        /// <param name="entity">entity get by this context</param>
        void Edit(TEntity entity);
        /// <summary>
        /// Delete entity change from db
        /// </summary>
        /// <param name="entity">entity get by this context</param>
        void Delete(TEntity entity);
        /// <summary>
        /// Returns the first element of a sequence that logically existed, or a default 
        /// value if the sequence contains
        /// </summary>
        /// <returns>default(TEntity) if source is empty or not existed in logical; otherwise, 
        /// the first element in source.</returns>
        TEntity FirstOrDefault();
        /// <summary>
        /// Returns the first element of a sequence that satisfies a specified condition and 
        /// logically existed, or a default value if the sequence contains
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>default(TEntity) if source is empty or not existed in logical; otherwise, 
        /// the first element in source.</returns>
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Returns the first element of a sequence including logical exist and activate check, 
        /// or a default value if the sequence contains
        /// </summary>
        /// <returns>default(TEntity) if source is empty or not existed in logical or existed 
        /// but inactivated; 
        /// otherwise, the first element in source.</returns>
        TEntity FirstOrDefaultActive();
        /// <summary>
        /// Returns the first element of a sequence that satisfies a specified condition, 
        /// logically existed and be activated, or a default value if the sequence contains
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>default(TEntity) if source is empty or not existed in logical; otherwise, 
        /// the first element in source.</returns>
        TEntity FirstOrDefaultActive(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Asynchronously returns the first element of a sequence, or a default value if
        /// the sequence contains no elements.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation. The task result contains default
        /// ( TEntity ) if source is empty or logicaly deleted; otherwise, the first element in source.</returns>
        Task<TEntity> FirstOrDefaultAsync();
        /// <summary>
        /// Asynchronously returns the first element of a sequence that satisfies a specified
        /// condition or a default value if no such element is found.
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains default
        /// ( TEntity ) if source is empty or logicaly deleted; otherwise, the first element in source.</returns>
        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Asynchronously returns the first element of a sequence, or a default value if
        /// the sequence contains no elements.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation. The task result contains default
        /// ( TEntity ) if source is empty or logicaly deleted or be inactivated; otherwise, the first 
        /// element in source.</returns>
        Task<TEntity> FirstOrDefaultActiveAsync();
        /// <summary>
        /// Asynchronously returns the first element of a sequence that satisfies a specified
        /// condition or a default value if no such element is found.
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>A task that represents the asynchronous operation. The task result contains default
        /// ( TEntity ) if source is empty or logicaly deleted or be inactivated; otherwise, the first 
        /// element in source.</returns>
        Task<TEntity> FirstOrDefaultActiveAsync(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted.
        /// </summary>
        /// <returns>Elements from the input sequence that logicaly not deleted.</returns>
        IQueryable<TEntity> GetAll();
        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted and be activated.
        /// </summary>
        /// <returns>Elements from the input sequence that logicaly not deleted and be activated.</returns>
        IQueryable<TEntity> GetAllActive();
        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted, be activated
        /// and satisfy the condition specified by predicate.
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>Elements from the input sequence that logicaly not deleted and be activated.</returns>
        IQueryable<TEntity> GetManyActive(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted and be approved.
        /// </summary>
        /// <returns>Elements from the input sequence that logicaly not deleted and be approved.</returns>
        IQueryable<TEntity> GetAllApproved();
        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted, be approved
        /// and satisfy the condition specified by predicate.
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>Elements from the input sequence that logicaly not deleted and be approved.</returns>
        IQueryable<TEntity> GetManyApproved(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Determines whether all the elements of a sequence that logicaly not deleted
        /// and satisfy the condition specified by predicate.
        /// </summary>
        /// <param name="predicate">Linq query</param>
        /// <returns>Elements from the input sequence that logicaly not deleted.</returns>
        IQueryable<TEntity> GetMany(Expression<Func<TEntity, bool>> predicate);
        /// <summary>
        /// Asynchronously finds an entity with the given primary key values. If an entity
        /// with the given primary key values exists in the context, then it is returned
        /// immediately without making a request to the store. Otherwise, a request is made
        /// to the store for an entity with the given primary key values and this entity,
        /// if found, is attached to the context and returned. If no entity is found in the
        /// context or the store, then null is returned.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="id">The values of the primary key for the entity to be found.</param>
        /// <returns>A task that represents the asynchronous find operation. The task result contains
        /// the entity found, or null.</returns>
        Task<TEntity> GetByIdAsync<TKey>(TKey id);
        /// <summary>
        /// Finds an entity with the given primary key values. If an entity with the given
        /// primary key values exists in the context, then it is returned immediately without
        /// making a request to the store. Otherwise, a request is made to the store for
        /// an entity with the given primary key values and this entity, if found, is attached
        /// to the context and returned. If no entity is found in the context or the store,
        /// then null is returned.
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="id">The values of the primary key for the entity to be found.</param>
        /// <returns>The entity found, or null.</returns>
        TEntity GetById<TKey>(TKey id);
        /// <summary>
        /// Reloads the entity from the database overwriting any property values with values
        /// from the database. The entity will be in the Unchanged state after calling this
        /// method.
        /// </summary>
        /// <param name="entity">entity</param>
        void Refresh(TEntity entity);
    }
}
