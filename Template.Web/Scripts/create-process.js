﻿function CreateProcess(createSuccessCallback) {
    this.createSuccessCallback = createSuccessCallback;
    this.CREATE_PROCESS_MODAL_ID = '#create-process-modal';
    this.CREATE_PROCESS_MODAL_SUBMIT_BUTTON_ID = '#create-process-modal [data-command=submit-new-process]';
    this.CREATE_PROCESS_FORM_ID = '#create-process-modal form';
    var popupSetting = this;

    this.submitCreateForm = function () {
        clearValidationMessage(popupSetting.CREATE_PROCESS_FORM_ID);
        $.ajax({
            url: $(popupSetting.CREATE_PROCESS_FORM_ID).attr('action'),
            method: 'POST',
            data: $(popupSetting.CREATE_PROCESS_FORM_ID).serialize(),
            success: function (res) {
                if (res.success) {
                    popupSetting.createSuccessCallback();
                    swal("Thành công",
                        res.msg,
                        "success");
                    $(popupSetting.CREATE_PROCESS_MODAL_ID).modal('hide');
                } else {
                    if (res.data) {
                        showValidationMessage(popupSetting.CREATE_PROCESS_FORM_ID, res.data);
                    } else {
                        swal("Thất bại",
                           res.msg,
                            "error");
                    }
                }
            },
            error: function (e) {
                swal("Thất bại",
                    "Có lỗi xẩy ra trong quá trình khởi tạo quy trình.",
                    "error");
            }
        });
    }

    $(this.CREATE_PROCESS_MODAL_SUBMIT_BUTTON_ID).on('click', this.submitCreateForm);
}

/**
 * author: TruongNLN
 * method: show create process dialog with inputed recruitment id and profile id or process id
 */
CreateProcess.prototype.showDialog = function (recruitmentId, profileId, processId) {
    $(this.CREATE_PROCESS_FORM_ID).empty();
    var popupSetting = this;
    $.ajax({
        url: $(popupSetting.CREATE_PROCESS_FORM_ID).attr('action'),
        method: 'GET',
        data: {
            recruitmentId: recruitmentId,
            profileId: profileId,
            processId: processId
        },
        success: function (res) {
            if (res && res.constructor === String) {
                $(popupSetting.CREATE_PROCESS_FORM_ID).html(res);
                var interviewerDropdownId = '{0} [name=Interviewers]'
                    .format(popupSetting.CREATE_PROCESS_FORM_ID)
                $(interviewerDropdownId)
                    .select2({
                        tags: true,
                        createTag: function (params) {
                            return undefined;
                        },
                        width: '100%'
                    });
                $(popupSetting.CREATE_PROCESS_MODAL_ID).modal('show');
            } else {
                swal("Thao tác không hợp lệ",
                   res.msg,
                    "error");
            }
        },
        error: function (e) {
            swal("Thao tác thất bại",
                "Có lỗi xẩy ra, vui lòng thử lại",
                "error");
        }
    });
}