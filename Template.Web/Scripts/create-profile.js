﻿(function () {

    var listCertification = [];
    var listCertificationType = [];
    var listCertificationAvailable = [];
    var listCertificationNotAvailable = [];
    var updatingCertification = {};

    $(document).ready(initialVariables);

    /**
     * author: HuyBT
     * callback: get data after page loaded
     */
    function initialVariables() {
        $("#tag").select2({
            tags: true,
            width: '100%'
        });
       
        $.ajax({
            method: 'POST',
            url: $('#certification-name').val(),
            success: function (res) {
                if (res.success) {
                    //store data into list
                    listCertificationType = res.data;

                    constructCertificationType(listCertificationType);
                    constructCertificationType_updateModal(listCertificationType);

                }
            }
        })

    }

    $('select[name=skills]').select2({
        tags: true,
        
    });

    /**
     * author: HuyBT
     * event:  
     */
    function selectedChange() {
        var result = $('#tags').select2('data');
        for (var i = 0; i < result.length; i++) {
            //if 
            if (result[i].selected == true) {

                $('#create-certifcation-modal [name=Duration]').prop('disabled', true);
                for (var j = 0; j < listCertificationType.length; j++) {
                    //if
                    if (result[i].text == listCertificationType[j].Name) {
                        $('#create-certifcation-modal [name=Duration]').val(listCertificationType[j].Duration);
                    }

                }

            }
            else {
                $('#create-certifcation-modal [name=Duration]').prop('disabled', false);
                $('#create-certifcation-modal [name=Duration]').val(' ');
            }
        }

    }

    function selectedChange_updateModal() {
        var result = $('#tag').select2('data');
        for (var i = 0; i < result.length; i++) {
            //if 
            if (result[i].selected == true) {

                $('#update-certifcation-modal [name=Duration]').prop('disabled', true);
                for (var j = 0; j < listCertificationType.length; j++) {
                    //if
                    if (result[i].text == listCertificationType[j].Name) {
                        $('#update-certifcation-modal [name=Duration]').val(listCertificationType[j].Duration);
                    }

                }

            }
            else {
                $('#update-certifcation-modal [name=Duration]').prop('disabled', false);
                $('#update-certifcation-modal [name=Duration]').val(' ');
            }
        }

    }

    $('#tags').select2().on('change', selectedChange);
    $('#tag').select2().on('change', selectedChange_updateModal);
    /**
    * author: HuyBT
    * method: show data in html when page load
    */
    function constructCertificationType(types) {
        $.each(types, function (i, e) {
            var optionTag = $('<option>', {
                value: e.TypeId,
                html: e.Name
            });
            $('#create-certifcation-modal [id=tags]').append(optionTag)


        });
        $("#tags").select2("val", "");
        $("#tags").select2({
            tags: true,
            width: '100%'
        });

    }
    function constructCertificationType_updateModal(types) {
        $.each(types, function (i, e) {
            var optionTag = $('<option>', {
                value: e.TypeId,
                html: e.Name
            });
            $('#update-certifcation-modal [id=tag]').append(optionTag)


        });
        $("#tag").select2("val", "");
        $("#tag").select2({
            tags: true,
            width: '100%'
        });

    }

    //
    function checkDuplicate(name) {
        for (var i = 0 ; i < listCertification.length; i++) {
            if (listCertification[i].Name == name) {
                return true;
            }
        }
        return false;
    }


    /**
    * author: HuyBT
    * event: call when user click "Lưu" button 
    */
    $('#create-new-certificationr').on('click', submitCreateCertification)

    function submitCreateCertification() {
        clearValidationMessage('#create-certifcation-modal');
        var selectedCertification = $('#create-certifcation-modal [id=tags]').select2('data');
        var name = "";
        var id = "";
        if (selectedCertification && selectedCertification.length > 0) {
            name = selectedCertification[0].text;
            id = selectedCertification[0].id;
        }
        var date = $('#create-certifcation-modal [name=CreateDate]').val();
        var duration = $('#create-certifcation-modal [name=Duration]').val();
        var note = $('#create-certifcation-modal [name=Note]').val();

        var errors = [];
        if (name.trim() == "") {
            errors.push({
                'Key': 'Name',
                'Value': 'Tên bằng cấp không thể để trống'
            });
        }
        else if (checkDuplicate(name)) {
            errors.push({
                'Key': 'Name',
                'Value': 'Tên bằng cấp đã tồn tại'
            });
        }

        if (date.trim() == "") {
            errors.push({
                'Key': 'CreateDate',
                'Value': 'Ngày Cấp không thể để trống'
            });
        }
        else if (validateDateString($('#create-certifcation-modal [name=CreateDate]').val(), 'dd/mm/yyyy') == false) {
            errors.push({
                'Key': 'CreateDate',
                'Value': 'Ngày Cấp không hợp lệ'
            });
        }

        if (!duration || duration.trim() == "") {
            errors.push({
                'Key': 'Duration',
                'Value': 'Thời gian hữu hiệu không thể để trống'
            });
        }

        if (errors.length != 0) {
            showValidationMessage('#create-certifcation-modal', errors);
        }
        else {

            certification = {
                'TypeId': id,
                'Name': name,
                'Date': date,
                'Duration': duration,
                'Note': note,
                'id': id,
                'text': name
            }
            for (var i = 0; i < selectedCertification.length; i++) {
                if (selectedCertification[i].selected == true) {
                    listCertificationAvailable.push(certification);
                }
                else {
                    listCertificationNotAvailable.push(certification);
                }
            }

            listCertification.push(certification);
            var buttons = $($('#certification-table-row-template').html());
            buttons.find('td:first-child').html(certification.Name)
            buttons.find('[data-command=delete-certification]').attr('data-name', certification.Name)
            buttons.find('[data-command=update-certification]').attr('data-name', certification.Name)
            $('#certification-table tbody').append(buttons);

            $('#create-certifcation-modal').modal('hide');

        }


    }

    /**
    * author: HuyBT
    * event: call when user click "Thêm mới" button 
    */
    $('#create-certification').on('click', ClickCreateCertification)

    function ClickCreateCertification() {
        clearValidationMessage('#create-certifcation-modal');
        $('#create-certifcation-modal [name=CreateDate]').val('');
        $('#create-certifcation-modal [name=Duration]').val('');
        $("#tags").select2("val", "");


    }

    $(document).on('click', '[data-command=delete-certification]', deleteCertificationButton_click);
    $(document).on('click', '[data-command=update-certification]', updateCertificationButton_click);

    function deleteCertificationButton_click() {
        var name = $(this).data('name');
        var removeElement = null;
        for (var i = 0 ; i < listCertification.length; i++) {
            if (listCertification[i].Name == name) {
                removeElement = listCertification[i];
                break;
            }
        }

        listCertificationNotAvailable
            = listCertificationNotAvailable.remove(removeElement);
        listCertificationAvailable =
            listCertificationAvailable.remove(removeElement);
        listCertification = listCertification.remove(removeElement);
        $(this).parents('tr').remove();
    }

    function updateCertificationButton_click() {
        $('#update-certifcation-modal').modal('show');
        $("#tag").select2({
            tags: true,
            width: '100%',
            data: listCertification
        });
        var name = $(this).data('name');

        for (var i = 0 ; i < listCertification.length; i++) {
            if (listCertification[i].Name == name) {
                $('#tag').val(listCertification[i].TypeId).trigger('change');

                for (var j = 0; j < listCertificationAvailable.length; j++) {
                    if (listCertificationAvailable[j].Name == listCertification[i].Name) {
                        $('#update-certifcation-modal [name=Duration]').prop('disabled', true);
                        $('#update-certifcation-modal [name=Duration]').val(listCertification[i].Duration);
                        break;
                    }
                }
                for (var j = 0; j < listCertificationNotAvailable.length; j++) {
                    if (listCertificationNotAvailable[j].Name == listCertification[i].Name) {
                        $('#update-certifcation-modal [name=Duration]').prop('disabled', false);
                        $('#update-certifcation-modal [name=Duration]').val(listCertification[i].Duration);
                        break;
                    }
                }

                $('#update-certifcation-modal [name=CreateDate]').val(listCertification[i].Date);
                $('#update-certifcation-modal [name=Note]').val(listCertification[i].Note);
                updatingCertification = listCertification[i];

                break;
            }


        }

    }

    function saveUpdateButton_click() {

    }

})();