/// <autosync enabled="true" />
/// <reference path="../content/js/charts.js" />
/// <reference path="../content/js/demo.js" />
/// <reference path="../content/js/flot-charts/bar-chart.js" />
/// <reference path="../content/js/flot-charts/curved-line-chart.js" />
/// <reference path="../content/js/flot-charts/dynamic-chart.js" />
/// <reference path="../content/js/flot-charts/line-chart.js" />
/// <reference path="../content/js/flot-charts/pie-chart.js" />
/// <reference path="../content/js/functions.js" />
/// <reference path="../content/js/global.js" />
/// <reference path="../content/js/ui-bootstrap-tpls-2.2.0.min.js" />
/// <reference path="../content/vendors/bootgrid/jquery.bootgrid.fa.js" />
/// <reference path="../content/vendors/bootgrid/jquery.bootgrid.js" />
/// <reference path="../content/vendors/bootstrap-growl/bootstrap-growl.min.js" />
/// <reference path="../content/vendors/bootstrap-wizard/jquery.bootstrap.wizard.min.js" />
/// <reference path="../Content/vendors/bower_components/autosize/dist/autosize.min.js" />
/// <reference path="../Content/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js" />
/// <reference path="../Content/vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.js" />
/// <reference path="../Content/vendors/bower_components/chosen/chosen.jquery.js" />
/// <reference path="../Content/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js" />
/// <reference path="../Content/vendors/bower_components/flot.curvedlines/curvedLines.js" />
/// <reference path="../Content/vendors/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js" />
/// <reference path="../Content/vendors/bower_components/flot/jquery.flot.js" />
/// <reference path="../Content/vendors/bower_components/flot/jquery.flot.pie.js" />
/// <reference path="../Content/vendors/bower_components/flot/jquery.flot.resize.js" />
/// <reference path="../Content/vendors/bower_components/flot-orderBars/js/jquery.flot.orderBars.js" />
/// <reference path="../Content/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js" />
/// <reference path="../Content/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.date.extensions.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.dependencyLib.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.extensions.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.numeric.extensions.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.phone.extensions.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/inputmask.regex.extensions.js" />
/// <reference path="../Content/vendors/bower_components/jquery.inputmask/jquery.inputmask.js" />
/// <reference path="../Content/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js" />
/// <reference path="../Content/vendors/bower_components/jquery/dist/jquery.min.js" />
/// <reference path="../Content/vendors/bower_components/jquery-ui/jquery-ui.js" />
/// <reference path="../Content/vendors/bower_components/lightgallery/light-gallery/js/lightGallery.min.js" />
/// <reference path="../Content/vendors/bower_components/mediaelement/build/mediaelement-and-player.js" />
/// <reference path="../Content/vendors/bower_components/moment/min/moment.min.js" />
/// <reference path="../Content/vendors/bower_components/nouislider/distribute/jquery.nouislider.all.min.js" />
/// <reference path="../Content/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js" />
/// <reference path="../Content/vendors/bower_components/summernote/dist/summernote.min.js" />
/// <reference path="../Content/vendors/bower_components/sweetalert/dist/sweetalert.min.js" />
/// <reference path="../Content/vendors/bower_components/ui-sortable/sortable.js" />
/// <reference path="../Content/vendors/bower_components/Waves/dist/waves.min.js" />
/// <reference path="../content/vendors/chosen_v1.4.2/chosen.jquery.min.js" />
/// <reference path="../content/vendors/farbtastic/farbtastic.min.js" />
/// <reference path="../content/vendors/fileinput/fileinput.min.js" />
/// <reference path="../content/vendors/input-file/js/fileinput.js" />
/// <reference path="../content/vendors/input-file/js/locales/ar.js" />
/// <reference path="../content/vendors/input-file/js/locales/bg.js" />
/// <reference path="../content/vendors/input-file/js/locales/ca.js" />
/// <reference path="../content/vendors/input-file/js/locales/cr.js" />
/// <reference path="../content/vendors/input-file/js/locales/cz.js" />
/// <reference path="../content/vendors/input-file/js/locales/da.js" />
/// <reference path="../content/vendors/input-file/js/locales/de.js" />
/// <reference path="../content/vendors/input-file/js/locales/el.js" />
/// <reference path="../content/vendors/input-file/js/locales/es.js" />
/// <reference path="../content/vendors/input-file/js/locales/fa.js" />
/// <reference path="../content/vendors/input-file/js/locales/fi.js" />
/// <reference path="../content/vendors/input-file/js/locales/fr.js" />
/// <reference path="../content/vendors/input-file/js/locales/hu.js" />
/// <reference path="../content/vendors/input-file/js/locales/id.js" />
/// <reference path="../content/vendors/input-file/js/locales/it.js" />
/// <reference path="../content/vendors/input-file/js/locales/ja.js" />
/// <reference path="../content/vendors/input-file/js/locales/kr.js" />
/// <reference path="../content/vendors/input-file/js/locales/kz.js" />
/// <reference path="../content/vendors/input-file/js/locales/lang.js" />
/// <reference path="../content/vendors/input-file/js/locales/nl.js" />
/// <reference path="../content/vendors/input-file/js/locales/pl.js" />
/// <reference path="../content/vendors/input-file/js/locales/pt.js" />
/// <reference path="../content/vendors/input-file/js/locales/pt-br.js" />
/// <reference path="../content/vendors/input-file/js/locales/ro.js" />
/// <reference path="../content/vendors/input-file/js/locales/ru.js" />
/// <reference path="../content/vendors/input-file/js/locales/sk.js" />
/// <reference path="../content/vendors/input-file/js/locales/sl.js" />
/// <reference path="../content/vendors/input-file/js/locales/th.js" />
/// <reference path="../content/vendors/input-file/js/locales/tr.js" />
/// <reference path="../content/vendors/input-file/js/locales/uk.js" />
/// <reference path="../content/vendors/input-file/js/locales/vi.js" />
/// <reference path="../content/vendors/input-file/js/locales/zh.js" />
/// <reference path="../content/vendors/input-file/js/locales/zh-tw.js" />
/// <reference path="../content/vendors/input-file/js/plugins/canvas-to-blob.js" />
/// <reference path="../content/vendors/input-file/js/plugins/purify.js" />
/// <reference path="../content/vendors/input-file/js/plugins/sortable.js" />
/// <reference path="../content/vendors/input-mask/input-mask.min.js" />
/// <reference path="../content/vendors/select2/js/select2.full.js" />
/// <reference path="../content/vendors/select2/js/select2.js" />
/// <reference path="../content/vendors/sparklines/jquery.sparkline.min.js" />
/// <reference path="create-process.js" />
/// <reference path="create-profile.js" />
/// <reference path="demo.js" />
/// <reference path="manage-career-category.js" />
/// <reference path="manage-careerCategory.js" />
/// <reference path="manage-career-category-detail.js" />
/// <reference path="manage-process.js" />
/// <reference path="manage-profile.js" />
/// <reference path="manage-recruitment.js" />
/// <reference path="manage-user.js" />
/// <reference path="select.js" />
/// <reference path="update-process.js" />
