﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.ViewModels
{
    public class AjaxResponseViewModel
    {
        public AjaxResponseViewModel()
        {
            this.success = false;
            this.data = null;
            this.msg = "";
        }
        public bool success { get; set; }
        public dynamic data { get; set; }
        public string msg { get; set; }
    }
}