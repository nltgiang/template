﻿using IPM.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.ViewModels
{
    public class UserViewModel : BaseEntityViewModel<AspNetUser>, IRowIndex
    {
        public UserViewModel() : base()
        {

        }

        public UserViewModel(AspNetUser user) : base(user)
        {
        }

        public int No { get; set; }
        public string Id { get; set; }
        public string Fullname { get; set; }
        public bool Active { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}