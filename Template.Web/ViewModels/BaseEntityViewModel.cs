﻿using AutoMapper;
using IPM.Service.Infrastructure;
using IPM.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.ViewModels
{
    public abstract class BaseEntityViewModel<T> where T : IEntity, new()
    {
        protected Type SelfType { get; set; }

        protected Type EntityType { get; set; }

        public BaseEntityViewModel()
        {
            this.SelfType = this.GetType();
            this.EntityType = typeof(T);
        }

        public BaseEntityViewModel(T entity) : this()
        {
            this.CopyFromEntity(entity);
        }

        public virtual void CopyFromEntity(T entity)
        {
            DependencyUtils.Resolve<IMapper>().Map(entity, this, this.EntityType, this.SelfType);
        }

        public virtual void CopyToEntity(T entity)
        {
            DependencyUtils.Resolve<IMapper>().Map(this, entity, this.SelfType, this.EntityType);
        }

        public T ToEntity()
        {
            T t = Activator.CreateInstance<T>();
            this.CopyToEntity(t);
            return t;
        }
    }
}