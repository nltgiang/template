﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using IPM.Service.Infrastructure;
using IPM.Service.Utils;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace IPM.Web.ViewModels
{
    public class BootgridResponseViewModel<T> where T : class, IRowIndex
    {
        private IQueryable<T> pagedResult;

        public BootgridResponseViewModel()
        {
        }

        public BootgridResponseViewModel(PagedQueryable<T> pagedQueryable)
        {
            this.current = pagedQueryable.Start + 1;
            this.rowCount = pagedQueryable.Items.Count();
            this.total = pagedQueryable.TotalQueriedItems;
            this.rows = pagedQueryable.Items;
        }

        public async Task SetModel<Entity>(PagedQueryable<Entity> pagedQueryable) where Entity : IEntity
        {
            this.current = pagedQueryable.Page;
            this.rowCount = pagedQueryable.Items.Count();
            this.total = pagedQueryable.TotalQueriedItems;
            var mappingConfig = DependencyUtils.Resolve<IConfigurationProvider>();
            var items = await pagedQueryable.Items.ToListAsync();
            this.rows = items.AsQueryable().ProjectTo<T>(mappingConfig);
        }

        public int current { get; set; }

        public int rowCount { get; set; }

        public IQueryable<T> rows
        {
            get
            {
                var items = this.pagedResult.ToList();
                var counter = this.current;
                foreach (var item in items)
                {
                    item.No = counter++;
                }
                return items.AsQueryable();
            }
            set
            {
                this.pagedResult = value;
            }
        }

        public int total { get; set; }

    }
}