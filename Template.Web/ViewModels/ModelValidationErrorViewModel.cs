﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Web.ViewModels
{
    public class ModelValidationErrorViewModel : List<KeyValuePair<string, string>>
    {
        public ModelValidationErrorViewModel()
        {

        }

        public ModelValidationErrorViewModel(IEnumerable<DbEntityValidationResult> entityValidationErrors)
        {
            foreach (var entity in entityValidationErrors.Where(a => !a.IsValid))
            {
                foreach (var error in entity.ValidationErrors)
                {
                    var name = string.Format("{0}.{1}", entity.Entry.Entity.GetType().Name, error.PropertyName);
                    Add(new KeyValuePair<string, string>(name, error.ErrorMessage));
                }
            }
        }

        public ModelValidationErrorViewModel(ModelStateDictionary modelState)
        {
            if (modelState.IsValid) return;

            foreach (var state in modelState)
            {
                if (modelState[state.Key].Errors.Any())
                {
                    foreach (var error in modelState[state.Key].Errors)
                    {
                        Add(new KeyValuePair<string, string>(state.Key, error.ErrorMessage));
                    }
                }
            }
        }
    }
}