﻿using System.Collections.Generic;
using System.Linq;

namespace IPM.Web.ViewModels
{
    public class BootgridRequestViewModel
    {
        public int current { get; set; }

        public int rowCount { get; set; }

        public string searchPhrase { get; set; }

        public Dictionary<string, string> sort { get; set; }

        public KeyValuePair<string, bool> FirstSortTerm
        {
            get
            {
                Dictionary<string, string> sortColumns = this.sort;
                if (sortColumns == null || sortColumns.Count == 0)
                {
                    return default(KeyValuePair<string, bool>);
                }
                KeyValuePair<string, string> keyValuePair = this.sort.First<KeyValuePair<string, string>>();
                return new KeyValuePair<string, bool>(keyValuePair.Key, keyValuePair.Value == "asc");
            }
        }
    }
}