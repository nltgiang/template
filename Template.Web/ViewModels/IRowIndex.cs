﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.ViewModels
{
    public interface IRowIndex
    {
        int No { get; set; }
    }
}