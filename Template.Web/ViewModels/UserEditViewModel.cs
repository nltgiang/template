﻿using IPM.Service.Entities;
using IPM.Web.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPM.Web.ViewModels
{
    public class UserEditViewModel : BaseEntityViewModel<AspNetUser>
    {
        public UserEditViewModel() : base()
        {

        }

        public UserEditViewModel(AspNetUser user) : base(user)
        {
        }

        public string Id { get; set; }

        [Required(ErrorMessage = ConstantManagement.User.REQUIRED_FULLNAME_VALIDATION_MESSAGE)]
        [RegularExpression(ConstantManagement.FULLNAME_REGEX,
            ErrorMessage = ConstantManagement.User.FULLNAME_FORMAT_VALIDATION_MESSAGE)]
        [StringLength(ConstantManagement.User.MAX_LENGTH_FULLNAME,
            MinimumLength = ConstantManagement.User.MIN_LENGTH_FULLNAME,
            ErrorMessage = ConstantManagement.User.FULLNAME_LENGTH_VALIDATION_MESSAGE)]
        public string Fullname { get; set; }

        [Required(ErrorMessage = ConstantManagement.User.REQUIRED_EMAIL_VALIDATION_MESSAGE)]
        [StringLength(ConstantManagement.User.MAX_LENGTH_EMAIL,
            MinimumLength = ConstantManagement.User.MIN_LENGTH_EMAIL,
            ErrorMessage = ConstantManagement.User.EMAIL_LENGTH_VALIDATION_MESSAGE)]
        [RegularExpression(ConstantManagement.EMAIL_REGEX,
            ErrorMessage = ConstantManagement.User.EMAIL_FORMAT_VALIDATION_MESSAGE)]
        public string Email { get; set; }

        [Required(ErrorMessage = ConstantManagement.User.REQUIRED_USERNAME_VALIDATION_MESSAGE)]
        [StringLength(ConstantManagement.User.MAX_LENGTH_USERNAME,
            MinimumLength = ConstantManagement.User.MIN_LENGTH_USERNAME,
            ErrorMessage = ConstantManagement.User.USERNAME_LENGTH_VALIDATION_MESSAGE)]
        [RegularExpression(ConstantManagement.USERNAME_REGEX, 
            ErrorMessage = ConstantManagement.User.USERNAME_FORMAT_VALIDATION_MESSAGE)]
        public string UserName { get; set; }

        [StringLength(ConstantManagement.User.MAX_LENGTH_PASSWORD,
            MinimumLength = ConstantManagement.User.MIN_LENGTH_PASSWORD,
            ErrorMessage = ConstantManagement.User.PASSWORD_LENGTH_VALIDATION_MESSAGE)]
        [Required(ErrorMessage = ConstantManagement.User.REQUIRED_PASSWORD_VALIDATION_MESSAGE)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = ConstantManagement.User.CONFIRM_PASSWORD_NOT_MATCH_VALIDATION_MESSAGE)]
        public string ConfirmPassword { get; set; }

        public bool IsHrRole { get; set; }

        public bool IsInterviewerRole { get; set; }
    }
}