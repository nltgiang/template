﻿using Autofac;
using Autofac.Builder;
using Autofac.Features.Scanning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.Modules
{
    public class RepositoryModule : Module
    {
        private System.Reflection.Assembly assembly;

        public RepositoryModule(System.Reflection.Assembly assembly)
        {
            this.assembly = assembly;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var repositoryBuilder = Autofac.RegistrationExtensions
                .RegisterAssemblyTypes(builder, new System.Reflection.Assembly[] { this.assembly });
            var scanningData = Autofac.RegistrationExtensions.Where(repositoryBuilder, this.RepositoryFilter);
            var registration = Autofac.RegistrationExtensions.AsImplementedInterfaces(scanningData);
            Autofac.RegistrationExtensions.InstancePerRequest(registration, new object[0]);
        }

        private bool RepositoryFilter(Type type)
        {
            return type.Name.EndsWith("Repository");
        }
    }
}