﻿using Autofac;
using IPM.Service.Infrastructure;
using IPM.Service.Utils;
using IPM.Web.Models;
using IPM.Web.Utils;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace IPM.Web.Modules
{
    public class EntityFrameworkModule : Module
    {
        private System.Reflection.Assembly assembly;

        private Type dbContextType;

        public EntityFrameworkModule(System.Reflection.Assembly assembly, Type dbContextType)
        {
            this.assembly = assembly;
            this.dbContextType = dbContextType;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(
                c => (HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>())
                    .FindById(HttpContext.Current.User.Identity.GetUserId())
                    .ToOperator())
                    .As<OperatorUser>()
                    .InstancePerRequest();

            RegistrationExtensions.InstancePerRequest(RegistrationExtensions
                .RegisterType(builder, this.dbContextType).As(new Type[]
                {
                    typeof(DbContext)
                }), new object[0]);
            RegistrationExtensions.InstancePerRequest(RegistrationExtensions
                .RegisterType(builder, typeof(UnitOfWork)).As(new Type[]
                {
                    typeof(IUnitOfWork)
                }), new object[0]);
        }
    }
}