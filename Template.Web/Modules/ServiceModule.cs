﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPM.Web.Modules
{
    public class ServiceModule: Module
    {
        private System.Reflection.Assembly assembly;

        public ServiceModule(System.Reflection.Assembly assembly)
        {
            this.assembly = assembly;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var repositoryBuilder = RegistrationExtensions
                .RegisterAssemblyTypes(builder, new System.Reflection.Assembly[] { this.assembly });
            var scanningData = RegistrationExtensions.Where(repositoryBuilder, this.RepositoryFilter);
            var registration = RegistrationExtensions.AsImplementedInterfaces(scanningData);
            RegistrationExtensions.InstancePerRequest(registration, new object[0]);
        }

        private bool RepositoryFilter(Type type)
        {
            return type.Name.EndsWith("Service");
        }
    }
}