﻿using Autofac;
using AutoMapper;
using IPM.Service;
using IPM.Service.Entities;
using IPM.Web.Models;
using IPM.Web.Utils;
using IPM.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Web.Modules
{
    public class AutoMapperModule : Module
    {
        private MapperConfiguration config;
        private const string DATE_FORMAT = "dd/MM/yyyy";
        private const string DATETIME_FORMAT = "dd/MM/yyyy HH:mm";

        public AutoMapperModule()
        {
            this.config = new MapperConfiguration(this.ConfigMapper);
        }

        protected override void Load(ContainerBuilder builder)
        {
            IMapper mapper = this.config.CreateMapper();
            RegistrationExtensions.RegisterInstance<MapperConfiguration>(builder, this.config)
                .As<IConfigurationProvider>().SingleInstance();
            RegistrationExtensions.RegisterInstance(builder, mapper)
                .As<IMapper>().SingleInstance();
            RegistrationExtensions.RegisterType(builder, typeof(UrlHelperFactory)).InstancePerRequest();
        }


        private void ConfigMapper(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<UserEditViewModel, ApplicationUser>()
                .ForMember(vm => vm.Id, opt => opt.Ignore());
            mapper.CreateMap<AspNetUser, UserViewModel>();
            mapper.CreateMap<AspNetUser, UserEditViewModel>()
                .ForMember(vm => vm.IsHrRole, opt => opt.MapFrom(m =>
                    m.AspNetRoles.Any(a => a.Name.Equals(ConstantManagement.HR_ROLE))))
                .ForMember(vm => vm.IsInterviewerRole, opt => opt.MapFrom(m =>
                    m.AspNetRoles.Any(a => a.Name.Equals(ConstantManagement.INTERVIEWER_ROLE))));
        }
    }
}