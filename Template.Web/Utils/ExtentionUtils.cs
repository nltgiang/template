﻿using IPM.Service.Infrastructure;
using IPM.Service.Utils;
using IPM.Web.ViewModels;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace IPM.Web.Utils
{
    public static class ExtentionUtils
    {
        public static T GetService<T>(this Controller controller) where T : class
        {
            return DependencyUtils.Resolve<T>();
        }

        public static string GetString(string stringCode)
        {
            var culture = WebConfigurationManager.AppSettings["Culture"];
            var baseName = string.Format("NHG.Inventory.App_GlobalResources.{0}", culture);
            var manager = new ResourceManager(baseName, Assembly.GetAssembly(typeof(ExtentionUtils)));
            var cultureInfo = CultureInfo.CreateSpecificCulture(culture);
            return manager.GetString(stringCode);
        }

        public static T GetAttribute<T>(this object value)
            where T : Attribute
        {

            PropertyInfo[] props = value.GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    T attrInstance = attr as T;
                    if (attrInstance != null)
                    {
                        return attrInstance;
                    }
                }
            }
            return null;
        }

        public static string GetString(this Controller controller, string stringCode)
        {
            return GetString(stringCode);
        }

        public static string GetString(this ValidationAttribute controller, string stringCode)
        {
            return GetString(stringCode);
        }

        public static string GetString(this Controller controller, string stringCode, params string[] param)
        {
            var messagePatterm = GetString(stringCode);
            if (string.IsNullOrWhiteSpace(messagePatterm))
            {
                return "";
            }
            return string.Format(messagePatterm, param);
        }

        public static string GetString(this HtmlHelper helper, string stringCode)
        {
            return GetString(stringCode);
        }

        public static string GetString(this HtmlHelper helper, string stringCode, params string[] param)
        {
            var messagePatterm = GetString(stringCode);
            if (string.IsNullOrWhiteSpace(messagePatterm))
            {
                return "";
            }
            return string.Format(messagePatterm, param);
        }

        public static IEnumerable<KeyValuePair<int, string>> GetValues<T>() where T : struct, IConvertible
        {
            var returnResult = new List<KeyValuePair<int, string>>();
            var values = Enum.GetValues(typeof(T)).Cast<T>();
            var enumName = typeof(T).Name;
            foreach (var value in values)
            {
                var enumValue = value as Enum;
                if (enumValue.GetAttributeOfType<IgnoreValueAttribute>() == null)
                {
                    var valueName = Enum.GetName(typeof(T), value);
                    var intValue = Convert.ToInt32(value);
                    var resourceName = string.Format("G_Enum_{0}_{1}", enumName, valueName);
                    var valueDisplay = GetString(resourceName);
                    if (string.IsNullOrWhiteSpace(valueDisplay))
                    {
                        valueDisplay = valueName;
                    }
                    var item = new KeyValuePair<int, string>(intValue, valueDisplay);
                    returnResult.Add(item);
                }
            }
            return returnResult;
        }
        /// <summary>
        /// Gets an attribute on an enum field value
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        public static Type GetGenericIEnumerablesType<T>(this IEnumerable<T> list)
        {
            return typeof(T);
        }

        public static void UpdateEntity<TViewModel, TModel>(this ICollection<TViewModel> viewModelList, ICollection<TModel> entityList)
            where TModel : IEntity, new()
            where TViewModel : BaseEntityViewModel<TModel>
        {
            var entityBaseFlag = false;
            if (entityList.Count > viewModelList.Count)
            {
                entityBaseFlag = true;
            }
            var entityListCount = entityList.Count;
            var viewModelListCount = viewModelList.Count;
            if (entityBaseFlag)
            {
                for (int i = 0; i < entityListCount; i++)
                {
                    var entity = entityList.ElementAt(i);
                    if (i < viewModelListCount)
                    {
                        viewModelList.ElementAt(i).CopyToEntity(entity);
                        if (entity is IDeletable)
                        {
                            (entity as IDeletable).Deleted = false;
                        }
                    }
                    else
                    {
                        if (entity is IDeletable)
                        {
                            (entity as IDeletable).Deleted = true;
                        }
                        else
                        {
                            entityList.Remove(entity);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < viewModelListCount; i++)
                {
                    var entity = entityList.ElementAtOrDefault(i);
                    var viewModel = viewModelList.ElementAt(i);
                    if (i < entityListCount)
                    {
                        viewModel.CopyToEntity(entity);
                        if (entity is IDeletable)
                        {
                            (entity as IDeletable).Deleted = false;
                        }
                    }
                    else
                    {
                        entity = viewModel.ToEntity();
                        entityList.Add(entity);
                    }
                }
            }
        }

        public static MvcHtmlString IpmValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            return helper.IpmValidationMessage(ExpressionHelper.GetExpressionText(expression));
        }

        public static MvcHtmlString IpmValidationMessage<TModel>(this HtmlHelper<TModel> helper, string name)
        {
            TagBuilder tagBuilder = new TagBuilder("small");
            tagBuilder.AddCssClass("help-block");
            tagBuilder.Attributes.Add("data-validation", name);
            var htmlString = tagBuilder.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(htmlString);
        }

        public static OperatorUser ToOperator(this IdentityUser user)
        {
            if (user == null)
            {
                return null;
            }
            else
            {
                return new OperatorUser
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    OperateTime = DateTime.Now
                };
            }
        }

        public static string ToString(this DateTime? date, string format)
        {
            if (date.HasValue)
            {
                return date.Value.ToString(format);
            }
            else
            {
                return "";
            }
        }
    }
}