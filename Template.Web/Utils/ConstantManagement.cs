﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace IPM.Web.Utils
{
    public static class ConstantManagement
    {
        public static string HR_ROLE
        {
            get
            {
                var roles = ConfigurationManager.AppSettings["Roles"].Split(';');
                return roles[1];
            }
        }

        public static string INTERVIEWER_ROLE
        {
            get
            {
                var roles = ConfigurationManager.AppSettings["Roles"].Split(';');
                return roles[2];
            }
        }

        public static string SA_ROLE
        {
            get
            {
                var roles = ConfigurationManager.AppSettings["Roles"].Split(';');
                return roles[0];
            }
        }

        public static string INSTALL_TOKEN
        {
            get { return ConfigurationManager.AppSettings["InstallToken"]; }
        }

        public static string[] SYSTEM_ROLES
        {
            get { return ConfigurationManager.AppSettings["Roles"].Split(';'); }
        }

        public static string[] SYSTEM_ADMIN_USERNAMES
        {
            get { return ConfigurationManager.AppSettings["SystemAdminUsername"].Split(';'); }
        }

        public static string SYSTEM_ADMIN_PASSWORD
        {
            get { return ConfigurationManager.AppSettings["SystemAdminPassword"]; }
        }

        public const string SA_DEFAULT_EMAIL = "sysadmin@techeco.net";

        public const string SA_DEFAULT_FULLNAME = "System administrator";

        public const string DEFAULT_VALIDATION_ERROR_MESSAGE = "Dữ liệu nhập vào chưa phù hợp";

        public const string USERNAME_REGEX = @"^\w+$";
        public const string EMAIL_REGEX = @"^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
        public const string FULLNAME_REGEX = @"^(?!.*\p{P}).*$";

        public static class Login
        {

            public const string WRONG_USERNAME_OR_PASSWORD_ERROR_MESSAGE
                = "Tên đăng nhập hoặc mật khẩu không đúng";
            public const string DEACTIVATED_ACCOUNT_ERROR_MESSAGE
                = "Tài khoản của bạn đã bị tạm khóa, vui lòng liên hệ với quản trị viên";

            public const string REQUIRED_USERNAME_VALIDATION_MESSAGE
                = "Tên đăng nhập không được để trống";
            public const string REQUIRED_PASSWORD_VALIDATION_MESSAGE
                = "Mật khẩu không được để trống";

        }


        public static class Process
        {
            public const string CREATE_PROCESS_SUCCESS_MESSAGE
                = "Tạo mới quy trình thành công";
            public const string CREATE_PROCESS_FAIL_MESSAGE
                = "Tạo mới quy trình thất bại";
            public const string DELETE_PROCESS_SUCCESS_MESSAGE
                = "Xóa quy trình thành công";
            public const string DELETE_PROCESS_FALSE_MESSAGE
                = "Xóa quy trình thất bại";
            public const string UPDATE_PROCESS_SUCCESS_MESSAGE
                = "Cập nhật quy trình thành công";
            public const string UPDATE_PROCESS_FAIL_MESSAGE
                = "Cập nhật quy trình thất bại";
            public const string PROCESS_NOT_FOUND_ERROR_MESSAGE
                = "Không thể tìm thấy quy trình đã chọn, vui lòng tải lại trang và thử lại";
            public const string UPDATE_PROCESS_CLOSED_ERROR_MESSAGE
                = "Bạn không thể cập nhật một quy trình đã đóng";
            public const string CAN_NOT_DELETE_PROCESS_MESSAGE
                = "Bạn không thể xóa một quy trình đã đóng";

            public const string PROFILE_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy hồ sơ liên quan, vui lòng tải lại trang và thử lại";
            public const string RECRUITMENT_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy vị trí tuyển dụng liên quan, vui lòng tải lại trang và thử lại";
            public const string PROFILE_LOCKED_ERROR_MESSAGE
                = "Ứng viên hiện tại đang tham gia vào một quy trình tuyển dụng khác, "
                + "khởi tạo quy trình tuyển dụng mới không thành công";
            public const string RECRUITMENT_CLOSED_ERROR_MESSAGE
                = "Bạn không thể khởi tạo quy trình tuyển dụng từ một vị trí đã ngưng tuyển dụng";
            public const string RECRUITMENT_EXPIRED_ERROR_MESSAGE
                = "Bạn không thể khởi tạo quy trình tuyển dụng từ một vị trí tuyển dụng đã quá hạn tuyển dụng";

            public const string REQUIRED_INTERVIEW_DATE_VALIDATION_MESSAGE = "Thời gian không được để trống";
            public const string REQUIRED_LOCATION_VALIDATION_MESSAGE = "Địa điểm không được để trống";
            public const string REQUIRED_INTERVIEWER_VALIDATION_MESSAGE = "Phải chọn ít nhất một phỏng vấn viên";
            public const string REQUIRED_RESULT_VALIDATION_MESSAGE = "Kết quả phỏng vấn không thể để trống";
            public const string REQUIRED_REPORT_VALIDATION_MESSAGE = "Chưa có báo cáo được ghi nhận";
            public const string INTERVIEW_DATE_FORMAT_VALIDATION_MESSAGE = "Ngày phỏng vấn không hợp lệ";
            public const string INTERVIEW_DATE_RANGE_VALIDATION_MESSAGE = "Ngày phỏng vấn không được ở quá khứ";
        }


        public static class User
        {
            public const int MIN_LENGTH_USERNAME = 1;
            public const int MAX_LENGTH_USERNAME = 128;
            public const int MIN_LENGTH_EMAIL = 1;
            public const int MAX_LENGTH_EMAIL = 256;
            public const int MIN_LENGTH_FULLNAME = 6;
            public const int MAX_LENGTH_FULLNAME = 100;
            public const int MIN_LENGTH_PASSWORD = 6;
            public const int MAX_LENGTH_PASSWORD = 100;

            public const string CREATE_USER_SUCCESS_MESSAGE = "Tạo tài khoản thành công";
            public const string CREATE_USER_FAIL_MESSAGE = "Tạo tài khoản thất bại";
            public const string UPDATE_USER_SUCCESS_MESSAGE = "Cập nhật tài khoản thành công";
            public const string UPDATE_USER_FAIL_MESSAGE = "Cập nhật tài khoản thất bại";

            public const string USER_NOT_FOUND_ERROR_MESSAGE = "Không tìm thấy người dùng này";
            public const string ACTIVATE_USER_SUCCESS_MESSAGE = "Kích hoạt tài khoản thành công.";
            public const string ACTIVATE_USER_FAIL_MESSAGE = "Kích hoạt tài khoản thất bại.";
            public const string DEACTIVATE_USER_SUCCESS_MESSAGE = "Vô hiệu hóa tài khoản thành công.";
            public const string DEACTIVATE_USER_FAIL_MESSAGE = "Vô hiệu hóa tài khoản thất bại.";

            //validation message
            public const string REQUIRED_USERNAME_VALIDATION_MESSAGE = "Tên đăng nhập không thể để trống";
            public const string DUPLICATED_USERNAME_VALIDATION_MESSAGE = "Tên đăng nhập đã tồn tại";
            public const string USERNAME_LENGTH_VALIDATION_MESSAGE = "Tên đăng nhập phải có độ dài từ {2}-{1} ký tự.";
            public const string USERNAME_FORMAT_VALIDATION_MESSAGE = "Tên đăng nhập không chứa ký tự đặc biệt";
            public const string REQUIRED_FULLNAME_VALIDATION_MESSAGE = "Họ và tên không thể để trống";
            public const string FULLNAME_LENGTH_VALIDATION_MESSAGE = "Họ và tên phải có độ dài từ {2}-{1} ký tự.";
            public const string FULLNAME_FORMAT_VALIDATION_MESSAGE = "Họ và tên không hợp lệ";
            public const string REQUIRED_EMAIL_VALIDATION_MESSAGE = "Email không thể để trống";
            public const string EMAIL_EXISTED_VALIDATION_MESSAGE = "Địa chỉ email đã tồn tại";
            public const string EMAIL_LENGTH_VALIDATION_MESSAGE = "Địa chỉ email phải có độ dài từ {2}-{1} ký tự.";
            public const string EMAIL_FORMAT_VALIDATION_MESSAGE = "Địa chỉ email không hợp lệ";
            public const string REQUIRED_PASSWORD_VALIDATION_MESSAGE = "Mật khẩu không thể để trống";
            public const string PASSWORD_LENGTH_VALIDATION_MESSAGE = "Mật khẩu phải có độ dài từ {2}-{1} ký tự.";
            public const string CONFIRM_PASSWORD_NOT_MATCH_VALIDATION_MESSAGE = "Xác nhận mật khẩu không trùng khớp";
            public const string MISSING_ROLE_VALIDATION_MESSAGE = "Phải chọn ít nhất 1 role";
        }
        public static class Profile
        {
            public const int MIN_LENGTH_FULLNAME = 1;
            public const int MAX_LENGTH_FULLNAME = 128;
            public const int MIN_LENGTH_PERSONALID = 9;
            public const int MAX_LENGTH_PERSONALID = 12;
            public const int MIN_LENGTH_PHONENUMBER = 8;
            public const int MAX_LENGTH_PHONENUMBER = 20;
            public const int MIN_LENGTH_LINKEDIN = 1;
            public const int MAX_LENGTH_LINKEDIN = 256;
            public const int MIN_LENGTH_EDUCATIONLV = 1;
            public const int MAX_LENGTH_EDUCATIONLV = 256;


            public const string CREATE_PROFILE_SUCCESS_MESSAGE = "Tạo hồ sơ thành công";
            public const string CREATE_PROFILE_FALSE_MESSAGE = "Tạo hồ sơ thất bại";
            public const string UPDATE_PROFILE_SUCCESS_MESSAGE = "Cập nhật hồ sơ thành công";
            public const string UPDATE_PROFILE_FALSE_MESSAGE = "Cập nhật hồ sơ thất bại";




            //validation message
            public const string REQUIRED_FULLNAME_VALIDATION_MESSAGE = "Họ và tên không thể để trống";
            public const string FULLNAME_FORMAT_VALIDATION_MESSAGE = "Họ và tên không hợp lệ";
            public const string REQUIRED_EMAIL_VALIDATION_MESSAGE = "Email không thể để trống";
            public const string EMAIL_FORMAT_VALIDATION_MESSAGE = "Email không hợp lệ";
            public const string EMAIL_EXISTED_VALIDATION_MESSAGE = "Email đã tồn tại";
            public const string REQUIRED_DOB_VALIDATION_MESSAGE = "Ngày tháng năm sinh không thể để trống";
            public const string DOB_FORMAT_VALIDATION_MESSAGE = "Ngày tháng năm sinh không hợp lệ";
            public const string AGE_SMALLER_THAN_18_MESSAGE = "Tuổi phải lớn hơn 18";
            public const string REQUIRED_PERSONALID_VALIDATION_MESSAGE = "Số CMND không thể bỏ trống";
            public const string PERSONALID_EXISTED_VALIDATION_MESSAGE = "Số CMND đã tồn tại";
            public const string PERSONALID_FORMAT_VALIDATION_MESSAGE = "Số CMND không hợp lệ";
            public const string REQUIRED_PHONENUMBER_VALIDATION_MESSAGE = "Số điện thoại không thể để trống";
            public const string PHONENUMBER_FORMAT_VALIDATION_MESSAGE = "Số điện thoại không hợp lệ";
            public const string REQUIRED_LINKEDIN_VALIDATION_MESSAGE = "LinkedIN không thể để trống";
            public const string REQUIRED_CV_VALIDATION_MESSAGE = "CV không thể để trống";
            public const string REQUIRED_EDUCATIONLV_VALIDATION_MESSAGE = "Trình độ học vấn không thể để trống";
            public const string FULLNAME_LENGTH_VALIDATION_MESSAGE = "Họ tên từ 1 - 128 ký tự";
            public const string PERSONALID_LENGTH_VALIDATION_MESSAGE = "Số CMND từ 9 - 12 số";
            public const string PHONENUMBER_LENGTH_VALIDATION_MESSAGE = "Số điện thoại từ 8 - 20 số";
            public const string LINKEDIN_LENGTH_VALIDATION_MESSAGE = "Số CMND từ 1 - 256 ký tự";
            public const string EDUCATIONLV_LENGTH_VALIDATION_MESSAGE = "Trình độ học vấn từ 1 - 256 ký tự";
            public const string REQUIRED_CERTIFICATIONNAME_VALIDATION_MESSAGE = "Tên bằng cấp không thể để trống";
            public const string CERTIFICATIONNAME_EXISTED_VALIDATION_MESSAGE = "Tên bằng cấp đã tồn tại";
            public const string REQUIRED_CreateDate_VALIDATION_MESSAGE = "Ngày cấp không thể để trống";
            public const string CreateDate_FORMAT_VALIDATION_MESSAGE = "Ngày cấp không hợp lệ";
            public const string DELETE_PROFILE_SUCCESS_MESSAGE
                = "Xóa hồ sơ thành công";
            public const string DELETE_PROFILE_FALSE_MESSAGE
                = "Xóa hồ sơ thất bại";
            public const string PROFILE_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy vị trí này";
            public const string PROFILE_DEACTIVE_ERROR_MESSAGE
                = "Ứng viên đang tham gia vào một quy trình tuyển dụng, không thể xóa hồ sơ ứng viên trong thời điểm hiện tại";
        }
        public static class CareerCategory
        {
            public const int MIN_LENGTH_CAREERCODE = 1;
            public const int MAX_LENGTH_CAREERCODE = 15;
            public const int MIN_LENGTH_CAREERNAME = 1;
            public const int MAX_LENGTH_CAREERNAME = 128;

            public const string CREATE_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Ngành nghề mới đã được thêm vào hệ thống";
            public const string CREATE_CAREER_CATEGORY_FALSE_MESSAGE
                = "Tạo nghành nghề thất bại";
            public const string DELETE_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Xóa nghành nghề thành công";
            public const string DELETE_CAREER_CATEGORY_FALSE_MESSAGE
                = "Xóa nghành nghề thất bại";
            public const string UPDATE_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Cập nhật nghành nghề thành công";
            public const string UPDATE_CAREER_CATEGORY_FALSE_MESSAGE
                = "Cập nhật nghành nghề thất bại";
            public const string CAREER_CATEGORY_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy ngành nghề này";

            //validation message for career category
            public const string REQUIRED_CAREER_CATEGORY_NAME_VALIDATION_MESSAGE
                = "Tên ngành nghề không thể để trống";
            public const string DUPLICATED_CAREER_CATEGORY_NAME_VALIDATION_MESSAGE
                = "Tên ngành nghề này đã tồn tại";
            public const string REQUIRED_CAREER_CATEGORY_CODE_VALIDATION_MESSAGE
                = "Mã ngành nghề không thể để trống";
            public const string DUPLICATED_CAREER_CATEGORY_CODE_VALIDATION_MESSAGE
                = "Mã ngành nghề này đã tồn tại";

            // validation message for sub career category
            public const string SUB_CAREER_CATEGORY_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy ngành con này";
            public const string CREATE_SUB_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Tạo mới nghành con thành công";
            public const string CREATE_SUB_CAREER_CATEGORY_FALSE_MESSAGE
                = "Tạo nghành con thất bại";
            public const string DELETE_SUB_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Xóa nghành con thành công";
            public const string DELETE_SUB_CAREER_CATEGORY_FALSE_MESSAGE
                = "Xóa nghành con thất bại";
            public const string UPDATE_SUB_CAREER_CATEGORY_SUCCESS_MESSAGE
                = "Cập nhật nghành nghề thành công";
            public const string UPDATE_SUB_CAREER_CATEGORY_FALSE_MESSAGE
                = "Cập nhật nghành con thất bại";
            public const string CAREER_SUB_CATEGORY_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy ngành con này";

            public const string REQUIRED_SUB_CAREER_CATEGORY_NAME_VALIDATION_MESSAGE
                = "Tên ngành con không thể để trống";
            public const string DUPLICATED_SUB_CAREER_CATEGORY_NAME_VALIDATION_MESSAGE
                = "Tên ngành con này đã tồn tại";
            public const string REQUIRED_SUB_CAREER_CATEGORY_CODE_VALIDATION_MESSAGE
                = "Mã ngành con không thể để trống";
            public const string DUPLICATED_SUB_CAREER_CATEGORY_CODE_VALIDATION_MESSAGE
                = "Mã ngành con này đã tồn tại";

            public const string NAME_CAREER_CATEGORY_LENGTH_VALIDATION_MESSAGE
                = "Tên ngành nghề phải có độ dài từ 1-128 ký tự";
            public const string CODE_CAREER_CATEGORY_LENGTH_VALIDATION_MESSAGE
                = "Mã ngành nghề phải có độ dài từ 1-15 ký tự";

            public const string NAME_SUB_CAREER_CATEGORY_LENGTH_VALIDATION_MESSAGE
                = "Tên ngành con phải có độ dài từ 1-128 ký tự";
            public const string CODE_SUB_CAREER_CATEGORY_LENGTH_VALIDATION_MESSAGE
                = "Mã ngành con phải có độ dài từ 1-15 ký tự";
        }

        public static class Recruitment
        {
            public const int MIN_LENGTH_CAREERNAME = 1;
            public const int MAX_LENGTH_CAREERNAME = 15;
            public const int MIN_LENGTH_CAREERCODE = 6;
            public const int MAX_LENGTH_CAREERCODE = 128;

            public const string CREATE_RECRUITMENT_CATEGORY_SUCCESS_MESSAGE
                = "Tạo mới vị trí thành công";
            public const string CREATE_RECRUITMENT_FALSE_MESSAGE
                = "Tạo mới vị trí thất bại";
            public const string DELETE_RECRUITMENT_SUCCESS_MESSAGE
                = "Xóa vị trí thành công";
            public const string CAN_NOT_DELETE_RECRUITMENT_MESSAGE
               = "Vị trí tuyển dụng đang trong quy trính tuyển dụng không thể xoá!";
            public const string DELETE_RECRUITMENT_FALSE_MESSAGE
                = "Xóa vị trí thất bại";
            public const string UPDATE_RECRUITMENT_SUCCESS_MESSAGE
                = "Cập nhật vị trí thành công";
            public const string UPDATE_RECRUITMENT_FALSE_MESSAGE
                = "Cập nhật vị trí thất bại";
            public const string RECRUITMENT_NOT_FOUND_ERROR_MESSAGE
                = "Không tìm thấy vị trí này";

            public const string REQUIRED_JOBTITLE_VALIDATION_MESSAGE
                 = "Tên vị trí không được để trống";
            public const string REQUIRED_CAREER_NAME_VALIDATION_MESSAGE
                             = "Tên ngành nghề không được để trống";
            public const string SALARYHIGHT_FORMAT_VALIDATION_MESSAGE
                = "Lương tối đa phải là số dương";
            public const string SALARYLOW_FORMAT_VALIDATION_MESSAGE
               = "Lương tối thiểu phải là số dương";
            public const string SALARYLOW_IS_BIGER_THAN_SALARYHIGHT_FORMAT_VALIDATION_MESSAGE
              = "Lương tối đa phải lớn hơn lương tối thiểu";
            public const string REQUIRED_STARTDATE_VALIDATION_MESSAGE
                            = "Ngày bắt đầu không được để trống";
            public const string STARTDATW_IS_APTER_DUEDATE_FORMAT_VALIDATION_MESSAGE
              = "Ngày kết thúc phải sau ngày bắt đầu";

        }


    }
}