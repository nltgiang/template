﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Configuration;

namespace IPM.Web.Utils
{

    public class NhgEntityNameAttribute : Attribute
    {
        public NhgEntityNameAttribute(Type entityType)
        {
            this.Name = entityType.Name;
        }

        public NhgEntityNameAttribute(string entityName)
        {
            this.Name = entityName;
        }

        public string Name { get; set; }
    }

    public class NhgDependenceRequireAttribute : NhgRequiredAttribute
    {
        public NhgDependenceRequireAttribute(string otherProperty, object dependenceValue,
            string messageCode = DEFAULT_MESSAGE_CODE, string nameCode = null) : base(messageCode, nameCode)
        {
            this.DependenceValue = dependenceValue;
            this.OtherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo property = validationContext.ObjectType.GetProperty(this.OtherProperty);
            object value2 = property.GetValue(validationContext.ObjectInstance, null);
            if (object.Equals(this.DependenceValue, value2))
            {
                return base.IsValid(value, validationContext);
            }
            return ValidationResult.Success;
        }

        public string OtherProperty { get; private set; }
        public object DependenceValue { get; private set; }
    }

    public class NhgRequiredAttribute : RequiredAttribute
    {
        public const string DEFAULT_MESSAGE_CODE = "G_ValidationError_Required";
        private string nameCode;
        private string entityName;

        public NhgRequiredAttribute() : this(DEFAULT_MESSAGE_CODE)
        {

        }
        public NhgRequiredAttribute(string messageCode) : this(messageCode, null)
        {

        }

        public NhgRequiredAttribute(string messageCode, string nameCode) : base()
        {
            this.AcceptZero = false;
            this.MessageCode = messageCode;
            this.NameCode = nameCode;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (string.IsNullOrEmpty(this.nameCode))
            {
                this.NameCode = validationContext.MemberName;
            }
            var attribute = value.GetAttribute<NhgEntityNameAttribute>();
            if (attribute != null)
            {
                this.entityName = attribute.Name;
            }
            else
            {
                this.entityName = "";
            }
            return base.IsValid(value, validationContext);
        }

        public override bool IsValid(object value)
        {
            if (value is int)
            {
                var intValue = (int)value;
                if (intValue == 0 && !this.AcceptZero)
                {
                    return false;
                }
            }
            return base.IsValid(value);
        }

        public override string FormatErrorMessage(string name)
        {
            if (this.MessageCode == null)
            {
                return base.FormatErrorMessage(name);
            }
            var errorMessage = this.GetString(this.MessageCode);
            var resourceName = "";
            if (string.IsNullOrWhiteSpace(this.entityName))
            {
                resourceName = string.Format("G_Display_{0}_{1}", this.entityName, this.nameCode);
            }
            else
            {
                resourceName = string.Format("G_Display_{0}", this.nameCode);
            }
            var modelDisplayName = this.GetString(resourceName);
            return string.Format(errorMessage, modelDisplayName);
        }

        public string MessageCode { get; set; }
        public bool AcceptZero { get; set; }
        public string NameCode
        {
            get
            {
                return this.nameCode;
            }
            set
            {
                this.nameCode = value;
            }
        }
    }
}