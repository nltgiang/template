﻿using IPM.Service.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Web.Utils
{
    public class UrlHelperFactory
    {
        public static UrlHelperFactory DefaultHelper
        {
            get
            {
                if (instance == null)
                {
                    instance = DependencyUtils.Resolve<UrlHelperFactory>();
                }
                return instance;
            }
        }

        private UrlHelper helper;
        private static UrlHelperFactory instance;

        public UrlHelper Helper
        {
            get
            {
                this.helper = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return this.helper;
            }
        }
    }
}