﻿using System.Web;
using System.Web.Optimization;

namespace IPM.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/VendorCss").Include(
                        "~/Content/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css",
                        "~/Content/vendors/bower_components/animate.css/animate.min.css",
                        "~/Content/vendors/bower_components/sweetalert/dist/sweetalert-override.min.css",
                        "~/Content/vendors/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css",
                        "~/Content/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css",
                        "~/Content/vendors/bower_components/chosen/chosen.min.css",
                        "~/Content/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css",
                        "~/Content/vendors/bootgrid/jquery.bootgrid.min.css",
                        "~/Content/vendors/socicon/socicon.min.css",
                        "~/Content/vendors/select2/css/select2.min.css",
                        "~/Content/vendors/input-file/css/fileinput.min.css"));
            bundles.Add(new StyleBundle("~/Content/AppCss").Include(
                        "~/Content/css/app.min.1.css",
                        "~/Content/css/app.min.2.css",
                        "~/Content/main-style.min.css"));
            bundles.Add(new StyleBundle("~/bundle/login-style").Include(
                        "~/Content/vendors/bower_components/animate.css/animate.min.css",
                        "~/Content/vendors/bower_components/material-design-iconic-font/css/material-design-iconic-font.min.css",
                        "~/Content/css/app.min.1.css",
                        "~/Content/css/app.min.2.css"));

            bundles.Add(new ScriptBundle("~/bundle/login-script").Include(
                        "~/Content/vendors/bower_components/jquery/dist/jquery.min.js",
                        "~/Content/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js",
                        "~/Content/vendors/bower_components/Waves/dist/waves.min.js",
                        "~/Content/js/functions.js"
                        ));
            bundles.Add(new ScriptBundle("~/Content/JavascriptLibraries").Include(
                        "~/Content/vendors/bower_components/jquery/dist/jquery.min.js",
                        "~/Content/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js",

                        "~/Content/vendors/bower_components/flot/jquery.flot.js",
                        "~/Content/vendors/bower_components/flot/jquery.flot.resize.js",
                        "~/Content/vendors/bower_components/flot.curvedlines/curvedLines.js",
                        "~/Content/vendors/sparklines/jquery.sparkline.min.js",
                        "~/Content/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js",

                        "~/Content/vendors/bower_components/moment/min/moment.min.js",
                        "~/Content/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.js",
                        "~/Content/vendors/bower_components/chosen/chosen.jquery.js",
                        "~/Content/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js",
                        "~/Content/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js",
                        "~/Content/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js",
                        "~/Content/vendors/bower_components/Waves/dist/waves.min.js",
                        "~/Content/vendors/bootstrap-growl/bootstrap-growl.min.js",
                        "~/Content/vendors/bower_components/sweetalert/dist/sweetalert.min.js",
                        "~/Content/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
                        "~/Content/vendors/bower_components/autosize/dist/autosize.min.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/inputmask.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/inputmask.date.extensions.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/inputmask.numeric.extensions.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/inputmask.regex.extensions.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/inputmask.phone.extensions.js",
                        "~/Content/vendors/bower_components/jquery.inputmask/jquery.inputmask.js",
                        "~/Content/vendors/bootgrid/jquery.bootgrid.min.js",
                        "~/Content/js/global.js"));

            bundles.Add(new ScriptBundle("~/Script/JqueryUI").Include(
                        "~/Content/vendors/bower_components/jquery-ui/jquery-ui.min.js"));
            
            bundles.Add(new ScriptBundle("~/bundle/manage-user").Include(
                        "~/Scripts/manage-user.js"));
            bundles.Add(new ScriptBundle("~/bundle/manage-process").Include(
                        "~/Content/vendors/input-file/js/plugins/canvas-to-blob.min.js",
                        "~/Content/vendors/input-file/js/plugins/sortable.min.js",
                        "~/Content/vendors/input-file/js/plugins/purify.min.js",
                        "~/Content/vendors/input-file/js/fileinput.min.js",
                        "~/Content/vendors/select2/js/select2.min.js",
                        "~/Scripts/update-process.js",
                        "~/Scripts/manage-process.js"));
            bundles.Add(new ScriptBundle("~/bundle/manage-career-category").Include(
                        "~/Scripts/manage-career-category.js"));
            bundles.Add(new ScriptBundle("~/bundle/manage-career-category-detail").Include(
                        "~/Scripts/manage-career-category-detail.js"));
            bundles.Add(new ScriptBundle("~/bundle/manage-profile").Include(
                        "~/Content/vendors/input-file/js/plugins/canvas-to-blob.min.js",
                        "~/Content/vendors/input-file/js/plugins/sortable.min.js",
                        "~/Content/vendors/input-file/js/plugins/purify.min.js",
                        "~/Content/vendors/input-file/js/fileinput.min.js",
                        "~/Content/vendors/select2/js/select2.min.js",
                        "~/Scripts/manage-profile.js"));
            bundles.Add(new ScriptBundle("~/bundle/demo").Include(
                        "~/Content/vendors/select2/js/select2.min.js",
                        "~/Scripts/demo.js"));

            bundles.Add(new ScriptBundle("~/bundle/manage-recruitment").Include(
                        "~/Content/vendors/select2/js/select2.min.js",
                        "~/Scripts/manage-recruitment.js"));
            bundles.Add(new ScriptBundle("~/bundle/create-profile").Include(
                        "~/Content/vendors/input-file/js/plugins/canvas-to-blob.min.js",
                        "~/Content/vendors/input-file/js/plugins/sortable.min.js",
                        "~/Content/vendors/input-file/js/plugins/purify.min.js",
                        "~/Content/vendors/input-file/js/fileinput.min.js",
                        "~/Content/vendors/select2/js/select2.min.js",
                        "~/Scripts/create-profile.js"));
        }
    }
}
