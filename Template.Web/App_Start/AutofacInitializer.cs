﻿using Autofac.Integration.Mvc;
using Autofac;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutofacMvc = Autofac.Integration.Mvc;
using Module = Autofac.Module;
using IPM.Web.Modules;
using System.Web.Mvc;
using IPM.Web.ViewModels;
using IPM.Service.Entities;
using IPM.Web.Utils;

namespace IPM.Web.App_Start
{
    public class AutofacInitializer
    {
        public static void Initialize(System.Reflection.Assembly assembly, Type dbContextType)
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();
            var controllerAssemblies = new System.Reflection.Assembly[] { assembly };
            AutofacMvc.RegistrationExtensions
                .RegisterControllers(containerBuilder, controllerAssemblies)
                .PropertiesAutowired(0);

            var serviceAssemply = Service.Utils.Utilities.GetPackageAssemply;
            ModuleRegistrationExtensions.RegisterModule(containerBuilder,
                new RepositoryModule(serviceAssemply));
            ModuleRegistrationExtensions.RegisterModule(containerBuilder,
                new ServiceModule(serviceAssemply));
            ModuleRegistrationExtensions.RegisterModule(containerBuilder,
                new EntityFrameworkModule(serviceAssemply, dbContextType));

            ModuleRegistrationExtensions.RegisterModule(containerBuilder, 
                new AutoMapperModule());
            var container = containerBuilder.Build(0);
            var resolver = new AutofacDependencyResolver(container);
            DependencyResolver.SetResolver(resolver);
        }
    }
}