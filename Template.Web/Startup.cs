﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IPM.Web.Startup))]
namespace IPM.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
