﻿using IPM.Web.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index()
        {
            if (this.User.IsInRole(ConstantManagement.HR_ROLE) || this.User.IsInRole(ConstantManagement.INTERVIEWER_ROLE))
            {
                return RedirectToAction("Index", "Process");
            }
            else if (this.User.IsInRole(ConstantManagement.SA_ROLE))
            {
                return RedirectToAction("Index", "User");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}