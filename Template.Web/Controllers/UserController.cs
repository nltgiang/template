﻿using AutoMapper;
using IPM.Service.Utils;
using IPM.Web.Models;
using IPM.Web.Utils;
using IPM.Web.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPM.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return this._userManager ?? this.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                this._userManager = value;
            }
        }
        
        public IMapper Mapper
        {
            get { return DependencyUtils.Resolve<IMapper>(); }
        }

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> CreateUser(UserEditViewModel viewModel)
        {
            var response = new AjaxResponseViewModel();
            //check validattion
            if (this.ModelState.IsValid)
            {
                var listRole = new List<string>();
                ModelValidationErrorViewModel errors = await ValidateNewUser(viewModel);
                //check all valid
                if (errors.Count == 0)
                {
                    try
                    {
                        //initial data
                        var user = new ApplicationUser();
                        this.Mapper.Map(viewModel, user);
                        user.Active = true;
                        if (viewModel.IsHrRole || viewModel.IsInterviewerRole)
                        {
                            //prepare user roles
                            if (viewModel.IsHrRole)
                            {
                                listRole.Add(ConstantManagement.HR_ROLE);
                            }
                            if (viewModel.IsInterviewerRole)
                            {
                                listRole.Add(ConstantManagement.INTERVIEWER_ROLE);
                            }
                        }
                        //create user account
                        var result = await this.UserManager.CreateAsync(user, viewModel.Password);
                        //check execute result
                        response.success = result.Succeeded;
                        if (result.Succeeded)
                        {
                            await this.UserManager.AddToRolesAsync(user.Id, listRole.ToArray());
                            response.msg = ConstantManagement.User.CREATE_USER_SUCCESS_MESSAGE;
                        }
                        else
                        {
                            response.msg = ConstantManagement.User.CREATE_USER_FAIL_MESSAGE;
                        }
                    }
                    catch (DbEntityValidationException exc)
                    {
                        //inputed value is invalid
                        response.data = new ModelValidationErrorViewModel(exc.EntityValidationErrors);
                        response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
                    }
                }
                else
                {
                    //inputed value is invalid
                    response.data = errors;
                    response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
                }
            }
            else
            {
                //inputed value is invalid
                response.data = new ModelValidationErrorViewModel(this.ModelState);
                response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
            }
            return Json(response);
        }

        private async System.Threading.Tasks.Task<ModelValidationErrorViewModel> ValidateNewUser(UserEditViewModel viewModel)
        {
            var errors = new ModelValidationErrorViewModel();
            //check existed username
            var existedUser = await this.UserManager.FindByNameAsync(viewModel.UserName);
            if (existedUser != null)
            {
                errors.Add(new KeyValuePair<string, string>("UserName",
                    ConstantManagement.User.DUPLICATED_USERNAME_VALIDATION_MESSAGE));
            }
            //check existed email
            existedUser = await this.UserManager.FindByEmailAsync(viewModel.Email);
            if (existedUser != null)
            {
                errors.Add(new KeyValuePair<string, string>("Email",
                    ConstantManagement.User.EMAIL_EXISTED_VALIDATION_MESSAGE));
            }
            //check at least exist a role
            if (!viewModel.IsHrRole && !viewModel.IsInterviewerRole)
            {
                errors.Add(new KeyValuePair<string, string>("Role",
                    ConstantManagement.User.MISSING_ROLE_VALIDATION_MESSAGE));
            }
            return errors;
        }
        
        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> UpdateUser(UserUpdateViewModel viewModel)
        {
            var response = new AjaxResponseViewModel();
            //check validattion
            if (this.ModelState.IsValid)
            {
                ModelValidationErrorViewModel errors = new ModelValidationErrorViewModel();
                var listRole = new List<string>();
                //check existed email
                var existedUser = await this.UserManager.FindByEmailAsync(viewModel.Email);
                if (existedUser != null && existedUser.Id != viewModel.Id)
                {
                    errors.Add(new KeyValuePair<string, string>("Email",
                        ConstantManagement.User.EMAIL_EXISTED_VALIDATION_MESSAGE));
                }
                //check at least exist a role
                if (!viewModel.IsHrRole && !viewModel.IsInterviewerRole)
                {
                    errors.Add(new KeyValuePair<string, string>("Role",
                        ConstantManagement.User.MISSING_ROLE_VALIDATION_MESSAGE));
                }
                else
                {
                    //prepare user roles
                    if (viewModel.IsHrRole)
                    {
                        listRole.Add(ConstantManagement.HR_ROLE);
                    }
                    if (viewModel.IsInterviewerRole)
                    {
                        listRole.Add(ConstantManagement.INTERVIEWER_ROLE);
                    }
                }
                if (!string.IsNullOrWhiteSpace(viewModel.Password))
                {
                    var passwordLength = viewModel.Password.Length;
                    if (passwordLength < ConstantManagement.User.MIN_LENGTH_PASSWORD
                        || passwordLength > ConstantManagement.User.MAX_LENGTH_PASSWORD)
                    {
                        var errorMessage = string.Format(ConstantManagement.User.PASSWORD_LENGTH_VALIDATION_MESSAGE
                            , ""
                            , ConstantManagement.User.MAX_LENGTH_PASSWORD
                            , ConstantManagement.User.MIN_LENGTH_PASSWORD);
                        errors.Add(new KeyValuePair<string, string>("Password", errorMessage));
                    }
                }
                //check all valid
                if (errors.Count == 0)
                {
                    try
                    {
                        //initial data
                        var user = await this.UserManager.FindByIdAsync(viewModel.Id);
                        user.Active = true;
                        user.Email = viewModel.Email;
                        user.Fullname = viewModel.Fullname;
                        if (!string.IsNullOrWhiteSpace(viewModel.Password))
                        {
                            user.PasswordHash = this.UserManager.PasswordHasher.HashPassword(viewModel.Password);
                        }
                        //create user account
                        var result = await this.UserManager.UpdateAsync(user);
                        //check execute result
                        response.success = result.Succeeded;
                        if (result.Succeeded)
                        {
                            await this.UserManager.RemoveFromRoleAsync(user.Id, ConstantManagement.HR_ROLE);
                            await this.UserManager.RemoveFromRoleAsync(user.Id, ConstantManagement.INTERVIEWER_ROLE);
                            await this.UserManager.AddToRolesAsync(user.Id, listRole.ToArray());
                            response.msg = ConstantManagement.User.UPDATE_USER_SUCCESS_MESSAGE;
                        }
                        else
                        {
                            response.msg = ConstantManagement.User.UPDATE_USER_FAIL_MESSAGE;
                        }
                    }
                    catch (DbEntityValidationException exc)
                    {
                        //inputed value is invalid
                        response.data = new ModelValidationErrorViewModel(exc.EntityValidationErrors);
                        response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
                    }
                }
                else
                {
                    //inputed value is invalid
                    response.data = errors;
                    response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
                }
            }
            else
            {
                //inputed value is invalid
                response.data = new ModelValidationErrorViewModel(this.ModelState);
                response.msg = ConstantManagement.DEFAULT_VALIDATION_ERROR_MESSAGE;
            }
            return Json(response);
        }
        
    }
}