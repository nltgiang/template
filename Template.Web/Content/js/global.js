﻿/****************************************************************************
 ******************************COMMON FUNCTIONS******************************
 ****************************************************************************/

window.constants = {
    bootgridCss: {
        icon: 'md icon',
        iconColumns: 'md-view-module',
        iconDown: 'md-expand-more',
        iconRefresh: 'md-refresh',
        iconSearch: 'glyphicon-search',
        iconUp: 'md-expand-less'
    }
};

/**
 * author: TruongNLN
 * method: generate string base on string format (e.g. "{0}")
 */
String.prototype.format = function (...arguments) {
    // The string containing the format items (e.g. "{0}")
    var thisValue = this.toString();

    if (arguments.length > 0) {
        if (arguments[0].constructor === Array) {
            arguments = [arguments[0]];
        }
    }
    for (var i = 0; i < arguments.length; i++) {
        // "gm" = RegEx options for Global search (more than one instance)
        // and for Multiline search
        var regEx = new RegExp("\\{" + (i) + "\\}", "gm");
        thisValue = thisValue.replace(regEx, arguments[i]);
    }

    return thisValue;
}

/**
 * author: TruongNLN
 * method: generate list key-value-pair for primitive list
 */
Array.prototype.toOptionArray = function () {
    var thisValue = this;
    for (var i = 0; i < thisValue.length; i++) {
        thisValue[i] = {
            'Key': thisValue[i],
            'Value': thisValue[i]
        }
    }
    return thisValue;
}

/**
 * author: TruongNLN
 * method: generate list key-value-pair for primitive list
 */
Array.prototype.remove = function (element) {
    var thisValue = this;
    thisValue = thisValue.filter(function (e, i) {
        return e != element;
    });
    return thisValue;
}

/**
 * author: TruongNLN
 * method: show notification message
 */
function notify(from, align, icon, type, animIn, animOut, title, message) {
    $.growl({
        icon: icon,
        title: title,
        message: message,
        url: ''
    }, {
        element: 'body',
        type: type,
        allow_dismiss: true,
        placement: {
            from: from,
            align: align
        },
        offset: {
            x: 20,
            y: 85
        },
        spacing: 10,
        z_index: 1031,
        delay: 4000,
        timer: 1000,
        url_target: '_blank',
        mouse_over: false,
        animate: {
            enter: animIn,
            exit: animOut
        },
        icon_type: 'class',
        template: $("#growl-template").html()
    });
};



/**
 * author: TruongNLN
 * method: show validation message
 */
function showValidationMessage(formId, errors) {
    $.each(errors, function (i, e) {
        var validationItem = '[data-validation={0}]'.format(e.Key);
        $(formId)
            .find(validationItem)
            .html(e.Value)
            .parents('.form-group')
            .addClass('has-error');
    })

}

/**
 * author: TruongNLN
 * method: show validation message
 */
function clearValidationMessage(formId) {
    $(formId)
        .find('[data-validation]')
        .html('')
        .parents('.form-group')
        .removeClass('has-error');

}


/**
 * author: TruongNLN
 * method: validate date string
 */
function validateDateString(value, pattern) {
    var elements = [];
    var date = 0;
    var month = 0;
    var year = 0;
    switch (pattern) {
        case 'dd/mm/yyyy':
            elements = value.split('/');
            if (elements.length != 3) {
                return false;
            }
            date = elements[0];
            month = elements[1];
            year = elements[2];
            break;
        default:
            return false;
    }

    var dateValue = new Date(year, month - 1, date);
    if (dateValue.getDate() != date) {
        return false;
    }
    if (dateValue.getMonth() != (month - 1)) {
        return false;
    }
    if (dateValue.getFullYear() != year) {
        return false;
    }
    return true;
}

